# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for finding an embedding of the largest complete graph in the Chimera graph """

from cge.funcs import embed, compress, cut


def get_max_complete_graph_embedding(chimera_hwa, max_rectangle_ratio=1.0, name=None):
    """
    get an embedding for the largest complete graph that is embeddable in the Chimera graph

    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :param (float) max_rectangle_ratio: parameter for applying the heuristic which specifies the allowed relative size
                                        of the rectangle caused by two broken vertices, should be in [0.0, 1.0],
                                        thus for a value of 1.0 all rectangles are allowed (no heuristic is applied) and
                                        for a value of 0.0 all rectangles are not allowed (might be too restrictive!)
    :param (str or None) name: name of the final embedding,
                               by default constructed from the name of the chimera_hwa and the max_rectangle_ratio
    :return: the embedding of the largest complete graph
    """
    embed.check_suitability(chimera_hwa)
    model = embed.get_embedding_model(chimera_hwa, max_rectangle_ratio)
    solution = model.solve()
    embedding = embed.extract_embedding(solution, chimera_hwa, name)
    return embedding


def get_max_complete_graph_embedding_by_compression(chimera_hwa, max_rectangle_ratio=1.0, name=None):
    """
    get an embedding for the largest complete graph that is embeddable in the compressed Chimera graph

    as the compression is already a heuristic dramatically reducing the problem complexity,
    it might not be useful to also use a very small max_rectangle_ratio here

    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :param (float) max_rectangle_ratio: parameter for applying the heuristic which specifies the allowed relative size
                                        of the rectangle caused by two broken vertices, should be in [0.0, 1.0],
                                        thus for a value of 1.0 all rectangles are allowed (no heuristic is applied) and
                                        for a value of 0.0 all rectangles are not allowed (might be too restrictive!)
    :param (str or None) name: name of the final embedding, by default constructed
    :return: the embedding of the largest complete graph obtained by compression
    """
    embed.check_suitability(chimera_hwa)
    compressed_chimera_hwa = compress.compress_chimera_hwa(chimera_hwa)
    compressed_embedding = get_max_complete_graph_embedding(compressed_chimera_hwa, max_rectangle_ratio)
    decompressed_embedding = compress.decompress_embedding(compressed_embedding, chimera_hwa, name)
    return decompressed_embedding


def get_min_cut_embedding(embedding, chimera_hwa, minimize_max_size=False, name=None):
    """
    get an embedding for the complete graph that is smaller than the given one
    by cutting redundant nodes from the crosses

    :param (quark.Embedding) embedding: the precalculated complete graph embedding based on crosses
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :param (bool) minimize_max_size: if True, the maximal size of a node set in the embedding is minimized,
                                     by default False, then the total number of nodes in the embedding is minimized
    :param (str or None) name: name of the final embedding,
                               by default constructed from the name of the embedding and the minimize_max_size option
    :return: the cut embedding of the complete graph
    """
    model = cut.get_cut_model(embedding, chimera_hwa, minimize_max_size)
    solution = model.solve()
    embedding = cut.extract_embedding(solution, chimera_hwa, name)
    return embedding
