# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for checking given crossroads or embeddings """

from itertools import product, combinations

from cge.chimera import Partition, Crossroad, Bounds
from cge.funcs.extract import get_crossroad


def are_valid_crossroads(crossroads, chimera_hwa):
    """
    check if the crossroads are valid and their corresponding maximal crosses can meet each other

    :param (list) crossroads: list of crossroads
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    """
    for i, crossroad1 in enumerate(crossroads):
        crossroad1 = Crossroad(*crossroad1)
        if is_out_of_bounds(crossroad1, chimera_hwa):
            return False
        for crossroad2 in crossroads[i+1:]:
            crossroad2 = Crossroad(*crossroad2)
            if are_in_same_row_or_col(crossroad1, crossroad2):
                return False
            if not crosses_meet(crossroad1, crossroad2, chimera_hwa):
                return False
    return True

def is_out_of_bounds(crossroad, chimera_hwa):
    """ check if the crossroad is in the bounds of the given hardware adjacency """
    return crossroad.row >= chimera_hwa.size.rows or crossroad.col >= chimera_hwa.size.cols \
           or crossroad.vert >= chimera_hwa.size.depth or crossroad.hori >= chimera_hwa.size.depth

def are_in_same_row_or_col(crossroad1, crossroad2):
    """ check if the crossroads are in the same row or column """
    return (crossroad1.row == crossroad2.row and crossroad1.vert == crossroad2.vert) \
           or (crossroad1.col == crossroad2.col and crossroad1.hori == crossroad2.hori)

def crosses_meet(crossroad1, crossroad2, chimera_hwa):
    """
    check if the crosses defined by the crossroads do meet in a unit cell,
    i.e., are not obstructed by missing nodes
    """
    min_col, max_col = min(crossroad1.col, crossroad2.col), max(crossroad1.col, crossroad2.col)
    min_row, max_row = min(crossroad1.row, crossroad2.row), max(crossroad1.row, crossroad2.row)

    if crosses_meet_horizontally(min_col, max_col, crossroad1.row, crossroad1.vert, chimera_hwa) \
            and crosses_meet_vertically(min_row, max_row, crossroad2.col, crossroad2.hori, chimera_hwa):
        return True
    if crosses_meet_horizontally(min_col, max_col, crossroad2.row, crossroad2.vert, chimera_hwa) \
            and crosses_meet_vertically(min_row, max_row, crossroad1.col, crossroad1.hori, chimera_hwa):
        return True
    return False

def crosses_meet_horizontally(left, right, row, vert, chimera_hwa):
    """ check if there is a broken node in the horizontal range """
    return not any(chimera_hwa.get_node(row, col, Partition.VERTICAL, vert) in chimera_hwa.missing_vertical_nodes
                   for col in range(left, right + 1))

def crosses_meet_vertically(top, bottom, col, hori, chimera_hwa):
    """ check if there is a broken node in the vertical range """
    return not any(chimera_hwa.get_node(row, col, Partition.HORIZONTAL, hori) in chimera_hwa.missing_horizontal_nodes
                   for row in range(top, bottom + 1))


def are_valid_bounds(bounds):
    """
    check if the crosses defined by the crossroads and restricted according to the bounds can meet each other
    (in an ideal Chimera graph, i.e., there is no check whether missing nodes are included)
    """
    for crossroad1, crossroad2 in combinations(bounds.keys(), 2):
        if do_not_meet(crossroad1, crossroad2, bounds):
            return False
    return True

def do_not_meet(crossroad1, crossroad2, bounds):
    """ check if the crosses defined by the crossroads but restricted according to the bounds meet each other """
    return do_not_meet_col1_row2(crossroad1, crossroad2, bounds) \
           and do_not_meet_col1_row2(crossroad2, crossroad1, bounds)

def do_not_meet_col1_row2(crossroad1, crossroad2, bounds):
    """
    check if the crosses defined by the crossroads but restricted according to the bounds meet each other
    via the column part of the first and the row part of the second cross
    """
    crossroad1, crossroad2 = Crossroad(*crossroad1), Crossroad(*crossroad2)
    bounds1, bounds2 = Bounds(*bounds[crossroad1]), Bounds(*bounds[crossroad2])
    # the rows and columns are enumerated from the left top(!) corner
    return bounds1.top > crossroad2.row or crossroad2.row > bounds1.bottom or \
           bounds2.left > crossroad1.col or crossroad1.col > bounds2.right

def is_valid_embedding(embedding, chimera_hwa):
    """
    check if the embedding is valid and all cross pairs do find a common unit cell

    :param (quark.Embedding) embedding: the precalculated complete graph embedding based on crosses
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    """
    if not embedding.is_valid(hwa=chimera_hwa):
        return False
    for nodes1, nodes2 in combinations(embedding.var_nodes_map.values(), 2):
        if not share_unit_cell(nodes1, nodes2, chimera_hwa):
            return False
    return True

def share_unit_cell(nodes1, nodes2, chimera_hwa):
    """ check whether the nodes find a common unit cell """
    for node1, node2 in product(nodes1, nodes2):
        coord1, coord2 = chimera_hwa.get_coordinate(node1), chimera_hwa.get_coordinate(node2)
        if coord1.row == coord2.row and coord1.col == coord2.col and coord1.part != coord2.part:
            return True
    return False

def form_cross(nodes, chimera_hwa):
    """ check whether the nodes form a cross """
    assert len(nodes) >= 2
    crossroad = get_crossroad(nodes, chimera_hwa)
    if not crossroad:
        return False
    crossroad_unit_cell = crossroad[:2]
    return all(not set(chimera_hwa.get_unit_cell(node)).isdisjoint(crossroad_unit_cell) for node in nodes)
