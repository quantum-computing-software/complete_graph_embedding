# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for extracting the final embedding from the solution """

from itertools import combinations

from quark import Solution, Embedding

from cge.chimera import Partition, Crossroad, Bounds


def extract_embedding_base(solution, extract_bounds_func, chimera_hwa, name=None):
    """
    get the embedding that corresponds to the activated crossroads in the solution

    :param (dict or list) solution: mapping of crossroads to values in {0, 1} telling whether crossroad is used
                                    or list of used crossroads
    :param (function) extract_bounds_func: function for the extraction of the bounds from the solution
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :param (str or None) name: name of the embedding,
                               by default the name of the solution or chimera_hwa
    :return: the embedding from the solution
    """
    bounds = extract_bounds_func(solution, chimera_hwa)
    var_nodes_map = get_var_nodes_map(bounds, chimera_hwa)
    name = name or solution.name if isinstance(solution, Solution) else None
    return Embedding.get_from_hwa(var_nodes_map, chimera_hwa, name)

def get_var_nodes_map(bounds, chimera_hwa):
    """
    get the nodes in the crosses of the complete graph embedding,
    those are defined by the activated crossroads given in the solution

    :param (dict[tuples]) bounds: crossroads to the bounds of the corresponding crosses
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :return: the lists of nodes defining the embedding
    """
    var_nodes_map = []
    for crossroad, cross_bounds in bounds.items():
        vert, hori = get_cross_nodes(crossroad, cross_bounds, chimera_hwa)
        var_nodes_map.append(sorted(vert + hori))
    return var_nodes_map

def get_cross_nodes(crossroad, cross_bounds, chimera_hwa):
    """
    get the nodes in the cross defined by the specific crossroad

    :param (Crossroad) crossroad: the edge in the Chimera graph defining a cross in the found embedding
    :param (tuple) cross_bounds: the unit cell coordinates defining the bounds of the cross
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :return: the nodes in the cross defining the embedding of a single original node
    """
    left, right, top, bottom = cross_bounds
    vert = [get_node_in_cross(i, crossroad, Partition.VERTICAL, chimera_hwa) for i in range(top, bottom + 1)]
    hori = [get_node_in_cross(i, crossroad, Partition.HORIZONTAL, chimera_hwa) for i in range(left, right + 1)]
    return vert, hori


def get_crosses_max_bounds(crossroads, chimera_hwa):
    """
    get the unit cell indices until which the crosses corresponding to the crossroads can be extended,
    due to missing nodes the crosses usually cannot be extended until the boundaries of the Chimera graph

    :param (list[Crossroad]) crossroads: the edges in the Chimera graph defining the crosses in the embedding
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :return: the mapping from the crossroads to the coordinate bounds of the cross defined by the crossroad
    """
    return {crossroad : get_cross_max_bounds(crossroad, chimera_hwa) for crossroad in crossroads}

def get_cross_max_bounds(crossroad, chimera_hwa):
    """
    get the unit cell indices until which the cross can be extended,
    due to missing nodes the crosses usually cannot be extended until the boundaries of the Chimera graph

    :param (Crossroad) crossroad: the edge in the Chimera graph defining a cross in the found embedding
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :return: the coordinate bounds of the cross defined by the crossroad
    """
    left, right = get_cross_max_bounds_1d(crossroad, Partition.HORIZONTAL, chimera_hwa)
    top, bottom = get_cross_max_bounds_1d(crossroad, Partition.VERTICAL, chimera_hwa)
    return Bounds(left, right, top, bottom)

def get_cross_max_bounds_1d(crossroad, hori_or_vert, chimera_hwa):
    """
    get the unit cell indices until which the cross can be extended in a certain direction

    :param (Crossroad) crossroad: the edge in the Chimera graph defining a cross in the found embedding
    :param (Partition) hori_or_vert: the desired dimension of the cross
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :return: the coordinate bounds of the cross in one dimension
    """
    crossroad = Crossroad(*crossroad)
    middle = crossroad.col if Partition.is_horizontal(hori_or_vert) else crossroad.row
    size = chimera_hwa.size.cols if Partition.is_horizontal(hori_or_vert) else chimera_hwa.size.rows
    missing_lower = [i for i in range(middle + 1) if is_missing(i - 1, crossroad, hori_or_vert, chimera_hwa)]
    missing_upper = [i for i in range(middle, size) if is_missing(i + 1, crossroad, hori_or_vert, chimera_hwa)]
    max_lower = max(missing_lower + [0])
    min_upper = min(missing_upper + [size - 1])
    return max_lower, min_upper

def is_missing(index, crossroad, hori_or_vert, chimera_hwa):
    """
    check if in the corresponding node in the cross is missing

    :param (int) index: number of the node in the corresponding dimension of the cross
    :param (Crossroad) crossroad: the edge in the Chimera graph defining a cross in the found embedding
    :param (Partition) hori_or_vert: the desired dimension of the cross
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :return: True if the corresponding node is missing
    """
    node = get_node_in_cross(index, crossroad, hori_or_vert, chimera_hwa)
    return node in chimera_hwa.get_missing_nodes(Partition.get_opposite(hori_or_vert))

def get_node_in_cross(index, crossroad, hori_or_vert, chimera_hwa):
    """
    get the corresponding node in the cross
    at the given unit cell column index in the row of the crossroad (if horizontal part)
    resp. at the given unit cell row index in the column of the crossroad (if vertical part)

    :param (int) index: number of the node in the corresponding dimension of the cross
    :param (Crossroad) crossroad: the edge in the Chimera graph defining a cross in the found embedding
    :param (Partition) hori_or_vert: the desired dimension of the cross
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :return: the number of the corresponding node
    """
    crossroad = Crossroad(*crossroad)
    if Partition.is_horizontal(hori_or_vert):
        return chimera_hwa.get_node(crossroad.row, index, Partition.VERTICAL, crossroad.vert)
    return chimera_hwa.get_node(index, crossroad.col, Partition.HORIZONTAL, crossroad.hori)


def recover_crossroads(embedding, chimera_hwa):
    """ extract the crossroads from the embedding (which should be based on crosses) """
    return sorted(get_crossroad(nodes, chimera_hwa) for nodes in embedding.var_nodes_map.values())

def get_crossroad(cross_nodes, chimera_hwa):
    """ extract the crossroad from the nodes of the embedding of a single original node (which should form a cross) """
    for node1, node2 in combinations(cross_nodes, 2):
        coord1, coord2 = chimera_hwa.get_coordinate(node1), chimera_hwa.get_coordinate(node2)
        if coord1.row == coord2.row and coord1.col == coord2.col:
            hori = coord1.dep if Partition.is_horizontal(coord1.part) else coord2.dep
            vert = coord2.dep if Partition.is_horizontal(coord1.part) else coord1.dep
            return Crossroad(coord1.row, coord1.col, vert, hori)
    return None

def recover_bounds(embedding, chimera_hwa):
    """ extract the crossroads and bounds from the embedding (which should be based on crosses) """
    return {get_crossroad(nodes, chimera_hwa): get_cross_bounds(nodes, chimera_hwa)
            for nodes in embedding.var_nodes_map.values()}

def get_cross_bounds(cross_nodes, chimera_hwa):
    """ extract the bounds from the nodes of the embedding of a single original node (which should form a cross) """
    vertical_nodes = set(cross_nodes).intersection(chimera_hwa.vertical_nodes)
    horizontal_nodes = set(cross_nodes).intersection(chimera_hwa.horizontal_nodes)
    left_coord = chimera_hwa.get_coordinate(min(vertical_nodes))
    right_coord = chimera_hwa.get_coordinate(max(vertical_nodes))
    top_coord = chimera_hwa.get_coordinate(min(horizontal_nodes))
    bottom_coord = chimera_hwa.get_coordinate(max(horizontal_nodes))
    return Bounds(left_coord.col, right_coord.col, top_coord.row, bottom_coord.row)
