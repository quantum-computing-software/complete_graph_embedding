# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for building up the SCIP optimization model to get the largest complete graph embedding """

from warnings import warn
from itertools import combinations
from pyscipopt import quicksum

from quark import ScipModel

from cge.utils.ranges import product_of_ranges_reusable
from cge.chimera import Partition, Crossroad
from cge.chimera.crossroad import get_all_crossroads, is_broken
from cge.chimera.mutually_exclusive_sets import get_mes_different, get_mes_both_equal, \
    get_commons_with_too_large_rectangles
from cge.funcs.extract import get_crosses_max_bounds, extract_embedding_base


def check_suitability(chimera_hwa):
    """
    check the suitability of the chimera hwa

    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    """
    if chimera_hwa.independent_missing_edges:
        warn("There are missing edges not being incident to missing vertices. "
             "This model cannot handle such edges. "
             "The obtained solution thus might not be valid for the Chimera graph. "
             "Please check solution afterwards "
             "and, if it is not valid, manually remove one of the incident nodes of each edge "
             "to circumvent missing edges. "
             "Rerun the optimization with this HWA.")

def get_embedding_model(chimera_hwa, max_rectangle_ratio=1.0, name=None):
    """
    get the SCIP optimization model searching for the largest complete graph in the ChimeraHWA

    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :param (float) max_rectangle_ratio: parameter for applying the heuristic which specifies the allowed relative size
                                        of the rectangle caused by two broken vertices, should be in [0.0, 1.0],
                                        thus for a value of 1.0 all rectangles are allowed (no heuristic is applied) and
                                        for a value of 0.0 all rectangles are not allowed (might be too restrictive!)
    :param (str or None) name: name of the model,
                               by default constructed from the name of the chimera_hwa and the max_rectangle_ratio
    :return: the constructed model
    """
    name = name or get_embedding_model_name(chimera_hwa, max_rectangle_ratio)
    model = ScipModel(name)

    # Variables
    add_variables(model, max_rectangle_ratio, chimera_hwa)

    # Objective
    add_objective(model)

    # Constraints
    add_matching_constraints(model, chimera_hwa)
    add_mutually_exclusive_sets_constraints(model, chimera_hwa)

    return model


def get_embedding_model_name(chimera_hwa, max_rectangle_ratio):
    """
    get the name of the SCIP model from the input

    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :param (float) max_rectangle_ratio: parameter for applying the heuristic
    :return: the name of the model
    """
    suffix = "" if max_rectangle_ratio >= 1.0 else f"_{float(max_rectangle_ratio)}"
    return chimera_hwa.name + suffix


def add_variables(model, max_rectangle_ratio, chimera_hwa):
    """
    add the variables and some fixed values to the model,
    where crossroads containing a broken node or excluded by the heuristic are still kept in the variable set
    but not redirected to a SCIP variable but rather directly set to the value 0

    :param (ScipModel) model: the SCIP model to be constructed
    :param (float) max_rectangle_ratio: parameter for applying the heuristic which specifies the allowed relative size
                                        of the rectangle caused by two broken vertices, should be in [0.0, 1.0],
                                        thus for a value of 1.0 all rectangles are allowed (no heuristic is applied) and
                                        for a value of 0.0 all rectangles are not allowed (might be too restrictive!)
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    """
    # heuristic approach disabling some crossroads in advance
    bad_crossroads = get_commons_with_too_large_rectangles(chimera_hwa, max_rectangle_ratio)
    model.data = {crossroad: model.addVar(f"x_{crossroad}", vtype="B")
                             if not (is_broken(crossroad, chimera_hwa) or crossroad in bad_crossroads) else 0
                  for crossroad in get_all_crossroads(chimera_hwa)}

def add_objective(model):
    """
    add the objective to the model

    :param (ScipModel) model: the SCIP model to be constructed
    """
    objective = quicksum(model.data.values())
    model.setObjective(objective)
    model.setMaximize()


def add_matching_constraints(model, chimera_hwa):
    """
    set up a simple matching problem as the base by adding the corresponding matching constraints,
    where we can only use one crossroad in each row and each column of the Chimera graph

    :param (ScipModel) model: the model for SCIP which is build up
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    """
    row_range = product_of_ranges_reusable(chimera_hwa.size.rows, chimera_hwa.size.depth)
    col_range = product_of_ranges_reusable(chimera_hwa.size.cols, chimera_hwa.size.depth)
    # only one crossroad in each:
    # row
    for row, dh in row_range():
        row_mes = [Crossroad(row, col, dh, dv) for col, dv in col_range()]
        add_constraint(model, f'matching_row_{row}_{dh}', row_mes)
    # column
    for col, dv in col_range():
        col_mes = [Crossroad(row, col, dh, dv) for row, dh in row_range()]
        add_constraint(model, f'matching_col_{col}_{dv}', col_mes)

def add_mutually_exclusive_sets_constraints(model, chimera_hwa):
    """
    add the constraints corresponding to mutually exclusive sets of crossroads
    which can not be used in the final embedding all at once due to missing qubits,
    there are different cases of missing qubits which need to be handled separately

    :param (ScipModel) model: the model for SCIP which is build up
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    """
    # two missing nodes of same kind
    add_constraints_both_equal(model, chimera_hwa)
    # two different missing nodes
    add_constraints_different(model, chimera_hwa)

def add_constraints_both_equal(model, chimera_hwa):
    """
    add the constraints for the mutually exclusive sets
    resulting from all combinations of missing nodes of equal type

    :param (ScipModel) model: the model for SCIP which is build up
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    """
    for part in Partition:
        for n1, n2 in combinations(chimera_hwa.get_missing_nodes(part), 2):
            name = f'both_{part.name}_{n1}_{n2}'
            mess = get_mes_both_equal(chimera_hwa, n1, n2)
            add_constraints(model, name, mess)

def add_constraints_different(model, chimera_hwa):
    """
    add the constraints for the mutually exclusive sets
    resulting from all combinations of missing nodes of different type

    :param (ScipModel) model: the model for SCIP which is build up
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    """
    for h in chimera_hwa.missing_horizontal_nodes:
        for v in chimera_hwa.missing_vertical_nodes:
            name = f'different_{h}_{v}'
            mess = get_mes_different(chimera_hwa, h, v)
            add_constraints(model, name, mess)


def add_constraint(model, constraint_name, mutually_exclusive_set):
    """
    add the constraint corresponding to a mutually exclusive set of crossroads to the model

    :param (ScipModel) model: the model for SCIP which is build up
    :param (str) constraint_name: the name of the constraint which is added for the MES
    :param (list[Crossroad]) mutually_exclusive_set: list of crossroads that cannot be assigned together
    """
    xs = model.data
    mes_vars = [xs[crossroad] for crossroad in mutually_exclusive_set if xs[crossroad]]
    if mes_vars:
        model.addCons(quicksum(mes_vars) <= 1, name=constraint_name)

def add_constraints(model, constraint_name, mutually_exclusive_sets):
    """
    add constraints for several mutually exclusive sets of crossroads to the model

    :param (ScipModel) model: the model for SCIP which is build up
    :param (str) constraint_name: the name of the constraint which is added for the MES
    :param (list[list[Crossroad]]) mutually_exclusive_sets: lists of list of crossroads that cannot be assigned together
    """
    for mes in mutually_exclusive_sets:
        add_constraint(model, constraint_name, mes)


def extract_bounds(solution, chimera_hwa):
    """
    get the bounds of the crosses corresponding to the activated crossroads in the solution

    :param (dict or list) solution: mapping of crossroads to values in {0, 1} telling whether crossroad is used
                                    or list of used crossroads
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :return: the mapping from crossroads to the corresponding cross bounds
    """
    if isinstance(solution, dict):
        # get the activated crossroads in the solution
        solution = sorted(crossroad for crossroad, value in solution.items() if abs(value - 1) < 1e-6)
    return get_crosses_max_bounds(solution, chimera_hwa)

def extract_embedding(solution, chimera_hwa, name=None):
    """
    get the embedding that corresponds to the activated crossroads in the solution

    :param (dict or list) solution: mapping of crossroads to values in {0, 1} telling whether crossroad is used
                                    or list of used crossroads
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :param (str or None) name: name of the embedding,
                               by default the name of the solution or chimera_hwa
    :return: the embedding from the solution
    """
    return extract_embedding_base(solution, extract_bounds, chimera_hwa, name)
