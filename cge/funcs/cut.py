# Copyright 2024 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for cutting the complete graph embedding """

from itertools import combinations
from pyscipopt import quicksum

from quark import ScipModel

from cge.chimera import Partition, Crossroad, Bounds
from cge.funcs.check import do_not_meet_col1_row2, do_not_meet
from cge.funcs.extract import recover_bounds, extract_embedding_base


def get_cut_model(embedding, chimera_hwa, minimize_max_size=False, name=None):
    """
    get the SCIP optimization model searching for the optimally cut embedding of the complete graph

    :param (quark.Embedding) embedding: the precalculated complete graph embedding based on crosses
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :param (bool) minimize_max_size: if True, the maximal size of a node set in the embedding is minimized,
                                     by default False, then the total number of nodes in the embedding is minimized
    :param (str or None) name: name of the model,
                               by default constructed from the name of the embedding and the minimize_max_size option
    :return: the constructed model
    """
    # TODO: add option to start with bounds directly
    name = name or get_cut_model_name(embedding, minimize_max_size)
    model = ScipModel(name)

    max_bounds = recover_bounds(embedding, chimera_hwa)
    min_bounds, double_option_pairs = get_min_bounds(max_bounds)
    ranges = get_ranges(max_bounds, min_bounds)

    # Variables
    add_variables(model, ranges)

    # Objective
    add_objective(model, max_bounds, minimize_max_size)

    # Constraints
    add_monotony_constraints(model, ranges)
    add_meeting_constraints(model, double_option_pairs)

    return model

def get_cut_model_name(embedding, minimize_max_length):
    """
    get the name of the SCIP model from the input

    :param (quark.Embedding) embedding:
    :param (bool) minimize_max_length:
    :return: the name of the model
    """
    suffix = "minimize_max_length" if minimize_max_length else ""
    return embedding.name + suffix


def get_min_bounds(max_bounds):
    """
    get the minimal bounds on the crosses respecting all pairs of crosses which do have only one possibility to meet

    :param max_bounds: the maximal bounds of the crosses given by broken nodes in the chimera hwa
    :return: the minimal bounds on the crosses
    """
    all_crossroads = [Crossroad(*crossroad) for crossroad in max_bounds.keys()]
    min_bounds = {crossroad: Bounds(crossroad.col, crossroad.col, crossroad.row, crossroad.row)
                  for crossroad in all_crossroads}
    double_option_pairs = []

    for c1, c2 in combinations(all_crossroads, 2):
        # if the maximal cross parts do not meet in one direction, they have to meet in the other direction
        # (as we assume we are given a valid embedding)
        # then we have to extend the minimal cross parts in the other direction
        if do_not_meet_col1_row2(c1, c2, max_bounds):
            min_bounds[c1], min_bounds[c2] = get_extended_bounds_row1_col2(c1, c2, min_bounds)
        elif do_not_meet_col1_row2(c2, c1, max_bounds):
            min_bounds[c2], min_bounds[c1] = get_extended_bounds_row1_col2(c2, c1, min_bounds)
        else:
            # if the cross meet in both directions, we have redundancy which can probably be cut off later
            double_option_pairs.append((c1, c2))

    double_option_pairs = [crossroads for crossroads in double_option_pairs if do_not_meet(*crossroads, min_bounds)]
    return min_bounds, double_option_pairs

def get_extended_bounds_row1_col2(crossroad1, crossroad2, min_bounds):
    """
    get the extended bounds in the row part of the cross corresponding to the first crossroad
    and in the column part of the cross corresponding to the second crossroad
    keeping the already set necessary bounds from other crossroad combinations
    """
    min_bounds1, min_bounds2 = min_bounds[crossroad1], min_bounds[crossroad2]
    left, right = min(min_bounds1.left, crossroad2.col), max(min_bounds1.right, crossroad2.col)
    top, bottom = min(min_bounds2.top, crossroad1.row), max(min_bounds2.bottom, crossroad1.row)
    return Bounds(left, right, min_bounds1.top, min_bounds1.bottom), \
           Bounds(min_bounds2.left, min_bounds2.right, top, bottom)

def get_ranges(max_bounds, min_bounds):
    """
    get the ranges for the variables indices
    the ranges are directed from the outer of the cross towards the crossroad
    """
    return {crossroad: Bounds(range(max_bounds[crossroad].left,   min_bounds[crossroad].left),
                              range(max_bounds[crossroad].right,  min_bounds[crossroad].right, -1),
                              range(max_bounds[crossroad].top,    min_bounds[crossroad].top),
                              range(max_bounds[crossroad].bottom, min_bounds[crossroad].bottom, -1))
            for crossroad in max_bounds.keys()}


def add_variables(model, all_ranges):
    """
    add the variables and some fixed values to the model

    :param (ScipModel) model: the SCIP model to be constructed
    :param all_ranges: the ranges corresponding to cross nodes which can either be in or outside the cut embedding
    """
    xs = {}
    for crossroad, ranges in all_ranges.items():
        for col in range(ranges.left.start, ranges.right.start + 1):
            var = (crossroad, Partition.VERTICAL, col)
            if col in ranges.left or col in ranges.right:
                xs[var] = model.addVar(f"x_{var[0]}_{var[1]}_{var[2]}", vtype='B')
            else:
                xs[var] = 1
        for row in range(ranges.top.start, ranges.bottom.start + 1):
            var = (crossroad, Partition.HORIZONTAL, row)
            if row in ranges.top or row in ranges.bottom:
                xs[var] = model.addVar(f"x_{var[0]}_{var[1]}_{var[2]}", vtype='B')
            else:
                xs[var] = 1
    model.data = xs

def add_objective(model, max_bounds, minimize_max_size):
    """
    add the objective to the model depending on the chosen variant

    :param (ScipModel) model: the SCIP model to be constructed
    :param max_bounds: the maximal bounds of the crosses given by broken nodes in the chimera hwa
    :param (bool) minimize_max_size: if True, the maximal size of a node set in the embedding is minimized,
                                     by default False, then the total number of nodes in the embedding is minimized
    """
    if minimize_max_size:
        objective = model.addVar("max_size")  # placeholder variable for maximum size of a node set in the embedding
        add_max_size_constraints(model, objective, max_bounds)
    else:
        objective = quicksum(model.data.values())

    model.setObjective(objective)
    model.setMinimize()

def add_max_size_constraints(model, max_size, max_bounds):
    """
    add the constraints that enforce the maximal size of the crosses to be minimized

    :param (ScipModel) model: the SCIP model to be constructed
    :param max_size: the placeholder variable saving the maximal size
    :param max_bounds: the maximal bounds of the crosses given by broken nodes in the chimera hwa
    """
    xs = model.data
    for crossroad, bounds in max_bounds.items():
        cross_size = sum(xs[(crossroad, Partition.VERTICAL, col)] for col in range(bounds.left, bounds.right + 1)) \
                     + sum(xs[(crossroad, Partition.HORIZONTAL, row)] for row in range(bounds.top, bounds.bottom + 1))
        model.addCons(max_size >= cross_size, f"max_size_>=_cross_size_{crossroad}")


def add_monotony_constraints(model, all_ranges):
    """
    add the constraints to the model enforcing that nodes inbetween an activated cross node and the crossroad are
    activated as well

    :param (ScipModel) model: the SCIP model to be constructed
    :param all_ranges: the ranges corresponding to cross nodes which can either be in or outside the cut embedding
    """
    for crossroad, ranges in all_ranges.items():
        add_monotony_constraint_range(model, crossroad, ranges.left, Partition.VERTICAL, 'left')
        add_monotony_constraint_range(model, crossroad, ranges.right, Partition.VERTICAL, 'right')
        add_monotony_constraint_range(model, crossroad, ranges.top, Partition.HORIZONTAL, 'top')
        add_monotony_constraint_range(model, crossroad, ranges.bottom, Partition.HORIZONTAL, 'bottom')

def add_monotony_constraint_range(model, crossroad, cross_range, hori_or_vert, part):
    """
    add the constraints to the model enforcing that nodes inbetween an activated cross node and the crossroad
    in the given range are activated as well

    :param (ScipModel) model: the SCIP model to be constructed
    :param crossroad: the inner unit cell edge connecting the vertical and horizontal part of the cross
    :param cross_range: the part of the cross where the elements with the smaller index need to be activated once an
                        element with a larger index is activated
    :param hori_or_vert: the part of the cross which is concerned
    :param part: the cross part
    """
    xs = model.data
    for k in cross_range[1:]:
        name = f'monotony_{part}_{crossroad}_{hori_or_vert}_{k}'
        model.addCons(xs[(crossroad, hori_or_vert, k - cross_range.step)] <= xs[(crossroad, hori_or_vert, k)], name)

def add_meeting_constraints(model, double_option_pairs):
    """
    add the constraints to the model enforcing that two crosses which have two options to meet,
    do actually meet in at least one

    :param (ScipModel) model: the SCIP model to be constructed
    :param double_option_pairs: the crossroads corresponding to crosses with redundancy to meet each other
    """
    xs = model.data
    for c1, c2 in double_option_pairs:
        lhs = xs[(c1, Partition.VERTICAL, c2.col)] * xs[(c2, Partition.HORIZONTAL, c1.row)] \
              + xs[(c1, Partition.HORIZONTAL, c2.row)] * xs[(c2, Partition.VERTICAL, c1.col)]
        model.addCons(lhs >= 1, f'crosses_meet_{c1}_{c2}')


def extract_bounds(solution, chimera_hwa=None):
    """
    get the bounds of the crosses corresponding to the activated crossroads in the solution

    :param (dict or list) solution: mapping of crossroads to values in {0, 1} telling whether crossroad is used
                                    or list of used crossroads
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :return: the mapping from crossroads to the corresponding cross bounds
    """
    solution = [key for key, val in solution.items() if abs(val - 1) < 1e-6]
    bounds = dict.fromkeys(crossroad for crossroad, _, _ in solution)
    for crossroad in bounds:
        cols = [col for crs, hov, col in solution if crs == crossroad and not Partition.is_horizontal(hov)]
        rows = [row for crs, hov, row in solution if crs == crossroad and Partition.is_horizontal(hov)]
        bounds[crossroad] = Bounds(min(cols), max(cols), min(rows), max(rows))
        assert bounds[crossroad].right <= chimera_hwa.size.rows
        assert bounds[crossroad].bottom <= chimera_hwa.size.cols
    return bounds

def extract_embedding(solution, chimera_hwa, name=None):
    """
    get the embedding that corresponds to the activated crossroads in the solution

    :param (dict or list) solution: mapping of ...
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :param (str or None) name: name of the embedding,
                               by default the name of the solution or chimera_hwa
    :return: the embedding from the solution
    """
    return extract_embedding_base(solution, extract_bounds, chimera_hwa, name)


def get_cut_embedding_heuristic(embedding, chimera_hwa):
    """
    get an embedding for the complete graph that is smaller than the given one by cutting redundant nodes
    from the crosses using a heuristic approach

    :param (quark.Embedding) embedding: the precalculated complete graph embedding based on crosses
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :return: the cut embedding of the complete graph
    """
    min_bounds = get_cut_bounds_heuristic(embedding, chimera_hwa)
    embedding = extract_embedding_base(min_bounds, lambda x, _: x, chimera_hwa)
    return embedding

def get_cut_bounds_heuristic(embedding, chimera_hwa):
    """
    get the bounds of an embedding heuristically via a greedy algorithm

    :param (quark.Embedding) embedding: the precalculated complete graph embedding based on crosses
    :param (cge.ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    """
    max_bounds = recover_bounds(embedding, chimera_hwa)
    min_bounds, double_option_pairs = get_min_bounds(max_bounds)
    for crossroad1, crossroad2 in double_option_pairs:
        if do_not_meet(crossroad1, crossroad2, min_bounds):
            new_bounds = get_cheapest_extension(crossroad1, crossroad2, min_bounds)
            min_bounds.update(new_bounds)
    return min_bounds

def get_cheapest_extension(crossroad1, crossroad2, bounds):
    """
    get new bounds such that both crosses corresponding to the crossroads meet at some point, chooses the minimal option

    :param crossroad1: the first crossroad
    :param crossroad2: the second crossroad
    :param bounds: the minimal bounds
    :return: the new bounds
    """
    crossroad1, crossroad2 = Crossroad(*crossroad1), Crossroad(*crossroad2)
    crossroad1, crossroad2 = (crossroad1, crossroad2) if crossroad1.row <= crossroad2.row else (crossroad2, crossroad1)
    bounds1 = Bounds(*bounds[crossroad1])
    bounds2 = Bounds(*bounds[crossroad2])

    if crossroad1.col <= crossroad2.col:
        costs1 = max(0, - crossroad1.row + bounds2.top   ) + max(0,   crossroad2.col - bounds1.right)
        costs2 = max(0,   crossroad2.row - bounds1.bottom) + max(0, - crossroad1.col + bounds2.left )
        if costs1 <= costs2:
            return {crossroad1: Bounds(bounds1.left, max(bounds1.right, crossroad2.col), bounds1.top, bounds1.bottom),
                    crossroad2: Bounds(bounds2.left, bounds2.right, min(bounds2.top, crossroad1.row), bounds2.bottom)}
        return {crossroad1: Bounds(bounds1.left, bounds1.right, bounds1.top, max(bounds1.bottom, crossroad2.row)),
                crossroad2: Bounds(min(bounds2.left, crossroad1.col), bounds2.right, bounds2.top, bounds2.bottom)}

    costs1 = max(0, - crossroad1.row + bounds2.top   ) + max(0, - crossroad2.col + bounds1.left )
    costs2 = max(0,   crossroad2.row - bounds1.bottom) + max(0,   crossroad1.col - bounds2.right)
    if costs1 <= costs2:
        return {crossroad1: Bounds(min(bounds1.left, crossroad2.col), bounds1.right, bounds1.top, bounds1.bottom),
                crossroad2: Bounds(bounds2.left, bounds2.right, min(bounds2.top, crossroad1.row), bounds2.bottom)}
    return {crossroad1: Bounds(bounds1.left, bounds1.right, bounds1.top, max(bounds1.bottom, crossroad2.row)),
            crossroad2: Bounds(bounds2.left, max(bounds2.right, crossroad1.col), bounds2.top, bounds2.bottom)}
