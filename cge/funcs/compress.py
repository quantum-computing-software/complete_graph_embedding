# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for compressing the ChimeraHWA to reduce problem size """

from quark import Embedding

from cge import ChimeraHWA


def compress_chimera_hwa(chimera_hwa, name=None):
    """
    get a compressed version of the ChimeraHWA with only a depth of one

    :param (ChimeraHWA) chimera_hwa: the Chimera hardware adjacency
    :param (str or None) name: name of the compressed ChimeraHWA,
                               by default the name of the chimera_hwa with suffix 'compressed'
    :return: the compressed ChimeraHWA with only a depth of one
    """
    size = (chimera_hwa.size.rows, chimera_hwa.size.cols, 1)
    missing = set()
    for node in chimera_hwa.missing_nodes:
        missing.add(int(node / chimera_hwa.size.depth))
    name = name or (chimera_hwa.name + '_compressed')
    return ChimeraHWA.get_without(missing, size, name)

def decompress_embedding(embedding, chimera_hwa, name=None):
    """
    get the uncompressed embedding by reverting the compression to fit to original ChimeraHWA

    :param (Embedding) embedding: the precalculated embedding for the compressed ChimeraHWA
    :param (ChimeraHWA) chimera_hwa: the original Chimera hardware adjacency
    :param (str or None) name: name of the decompressed embedding,
                               by default the name of the embedding with suffix 'decompressed'
    :return: the corresponding embedding for the original ChimeraHWA
    """
    var_nodes_map = []
    for old_nodes in embedding.var_nodes_map.values():
        for dep in range(chimera_hwa.size.depth):
            new_nodes = [node * chimera_hwa.size.depth + dep for node in old_nodes]
            var_nodes_map.append(new_nodes)
    name = name or (embedding.name + '_decompressed')
    return Embedding.get_from_hwa(var_nodes_map, chimera_hwa, name)
