# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for helper functions for ranges """

from itertools import product


def product_of_ranges(*values):
    """ get the product of the ranges over the given values """
    return product(*[range(val) for val in values])

def product_reusable(*ranges):
    """ workaround to reuse iterator """
    return lambda: product(*ranges)

def product_of_ranges_reusable(*vals):
    """ workaround to reuse iterator """
    return lambda: product_of_ranges(*vals)

def get_relative_range(value, anchor, size):
    """ range according to relative position of the two input parameters """
    return range(*get_relative_start_end(value, anchor, size))

def get_relative_start_end(value, anchor, size):
    """ start and end of range according to relative position of the two input parameters """
    return int(value) * (value > anchor), (int(value) + 1) * (value <= anchor) + size * (value > anchor)

def get_length_range(value, anchor, size):
    """ relative difference of given values """
    start, end = get_relative_start_end(value, anchor, size)
    return end - start
