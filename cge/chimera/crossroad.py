# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for crossroad and corresponding helper functions """

from collections import namedtuple

from cge.utils.ranges import product_of_ranges
from .indexing import Partition, Coordinate


Crossroad = namedtuple('Crossroad', ['row', 'col', 'vert', 'hori'])
Bounds = namedtuple('Bounds', ['left', 'right', 'top', 'bottom'])

def is_broken(crossroad, chimera_hwa):
    """
    check if one of the concerned nodes is missing

    :param (Crossroad) crossroad: the edge in the hardware graph defining a cross in the found embedding
    :param (ChimeraHWA) chimera_hwa: the Chimera graph hardware adjacency
    :return: True if the crossroad contains a missing node
    """
    crossroad = Crossroad(*crossroad)
    node_hori = chimera_hwa.get_node(crossroad.row, crossroad.col, Partition.HORIZONTAL, crossroad.hori)
    node_vert = chimera_hwa.get_node(crossroad.row, crossroad.col, Partition.VERTICAL, crossroad.vert)
    return node_hori in chimera_hwa.missing_horizontal_nodes or node_vert in chimera_hwa.missing_vertical_nodes

def get_all_crossroads(chimera_hwa):
    """
    get all the crossroads of a certain hwa where both vertices are non-broken

    :param (ChimeraHWA) chimera_hwa: the Chimera graph hardware adjacency
    :return: the list of crossroads
    """
    return [Crossroad(*tup) for tup in product_of_ranges(*chimera_hwa.size, chimera_hwa.size.depth)]

def get_common_crossroad(coord1, coord2):
    """
    get the common crossroad defined by two missing nodes

    :param (Coordinate) coord1: the coordinates of the first missing node
    :param (Coordinate) coord2: the coordinates of the second missing node
    :return: the common crossroad
    """
    coord1, coord2 = Coordinate(*coord1), Coordinate(*coord2)
    if coord1.part == coord2.part:
        raise ValueError('The nodes should have different orientations')
    if Partition.is_horizontal(coord1.part):
        return Crossroad(coord2.row, coord1.col, coord2.dep, coord1.dep)
    return Crossroad(coord1.row, coord2.col, coord1.dep, coord2.dep)
