# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for helper objects and functions for defining the size of the Chimera graph """

import inspect
from collections import namedtuple
from collections.abc import Iterable
from functools import wraps


Size = namedtuple('Size', ['rows', 'cols', 'depth'])

def get_size(rows, cols=None, depth=4):
    """
    kind of constructor method for size with defaults
    if no number of columns is given, it will default to the number of rows, thus a symmetric graph
    by default we have a depth of 4 according to currently operating hardware
    """
    cols = cols or rows
    return Size(rows, cols, depth)

def _format_size(size):
    """ the size can be given as single int or iterable of maximum length 3 """
    if isinstance(size, Iterable):
        size = tuple(size)
    if not isinstance(size, tuple):
        size = (size,)
    return get_size(*size)

def default_size(func):
    """ decorator setting the default size """
    argspec = inspect.getfullargspec(func)

    @wraps(func)
    def wrapper(*args, **kwargs):
        """ takes given size input and passes correct size possibly with defaults """
        passed_args = argspec.args[:len(args)]
        if 'size' in passed_args:
            i = passed_args.index('size')
            args = args[:i] + (_format_size(args[i]),) + args[i + 1:]
        elif 'size' in kwargs:
            kwargs['size'] = _format_size(kwargs['size'])
        return func(*args, **kwargs)
    return wrapper
