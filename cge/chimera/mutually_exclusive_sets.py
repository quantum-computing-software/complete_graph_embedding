# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for assembling the mutually exclusive sets of crossroads """

from cge.utils.ranges import get_length_range, get_relative_range, product_reusable
from .crossroad import Crossroad, Partition, get_common_crossroad


# methods to calculate sizes of the rectangles corresponding to two broken vertices
# mainly used for the heuristic step to remove the corresponding constraints in advance
# less computational effort than providing the sets

def get_commons_with_too_large_rectangles(chimera_hwa, max_rectangle_ratio=1.0):
    """
    some commons of two broken nodes of different type cause very large rectangles of mutually exclusive sets,
    a heuristic approach is removing some of them in advance, if this rectangle is too large,
    where a ratio of 1 means no crossroad is removed, 0 means all are

    :param (ChimeraHWA) chimera_hwa: the Chimera graph hardware adjacency
    :param (float) max_rectangle_ratio: parameter for applying the heuristic which specifies the allowed relative size
                                        of the rectangle caused by two broken vertices, should be in [0.0, 1.0]
                                        thus for a value of 1.0 all rectangles are allowed (no heuristic is applied) and
                                        for a value of 0.0 all rectangles are not allowed (might be too restrictive!)
    :return: the common crossroads that cause too large forbidden rectangles
    """
    if max_rectangle_ratio >= 1:
        return []
    max_size = max_rectangle_ratio * chimera_hwa.size.rows * chimera_hwa.size.cols
    common_2_size = get_rectangle_sizes(chimera_hwa)
    return [common for common, size in common_2_size.items() if size > max_size]

def get_rectangle_sizes(chimera_hwa):
    """
    for all combinations of vertical and horizontal vertices
    get the size of the rectangle of mutually exclusive crossroads

    :param (ChimeraHWA) chimera_hwa: the Chimera graph hardware adjacency
    :return: the mapping of common crossroads to the size of the rectangles which they exclude
    """
    common_2_size = {}
    for h in chimera_hwa.missing_horizontal_nodes:
        for v in chimera_hwa.missing_vertical_nodes:
            common, size = get_size_mes_different(chimera_hwa, h, v)
            # two different pairs of vertices can have the same common crossroad
            # for which we only use the largest corresponding rectangle
            common_2_size[common] = max(common_2_size.get(common, 0), size)
    return common_2_size

def get_size_mes_different(chimera_hwa, node1, node2):
    """
    get the size of the rectangle caused by the two broken vertices

    :param (ChimeraHWA) chimera_hwa: the Chimera graph hardware adjacency
    :param (int) node1: number of first node
    :param (int) node2: number of second node
    :return: the size of the rectangle
    """
    hori, vert = get_horizontal_and_vertical_coordinates(chimera_hwa, node1, node2)
    common = get_common_crossroad(hori, vert)
    if is_in_same_uc_row_or_column(hori, vert):
        # no constraints are necessary
        return common, 0
    rows, cols = _get_size_rectangle(chimera_hwa, hori, vert)
    return common, rows * cols

def _get_size_rectangle(chimera_hwa, hori, vert):
    """ get the dimensions of the rectangle """
    num_rows = _get_length_rectangle(chimera_hwa, hori, vert, Partition.HORIZONTAL)
    num_cols = _get_length_rectangle(chimera_hwa, vert, hori, Partition.VERTICAL)
    return num_rows, num_cols

def _get_length_rectangle(chimera_hwa, hori, vert, hori_or_vert):
    """ get the size in one dimension """
    if Partition.is_horizontal(hori_or_vert):
        return get_length_range(hori.row, vert.row, chimera_hwa.size.rows)
    return get_length_range(hori.col, vert.col, chimera_hwa.size.cols)


# get the sets of mutually exclusive crossroads defined by two broken vertices of the same type

def get_mes_both_equal(chimera_hwa, node1, node2):
    """
    get the mutually exclusive sets for two missing nodes of same type

    :param (ChimeraHWA) chimera_hwa: the Chimera graph hardware adjacency
    :param (int) node1: number of first node
    :param (int) node2: number of second node
    :return: lists of nodes forming the mutually exclusive sets
    """
    coord1 = chimera_hwa.get_coordinate(node1)
    coord2 = chimera_hwa.get_coordinate(node2)
    if coord1.part != coord2.part:
        raise ValueError('The nodes should have the same orientations')
    if is_in_same_inner_row_or_col(coord1, coord2):
        # the constraint is already caught by the matching constraints
        return []
    range1 = _get_relative_size_range(chimera_hwa, coord1, coord2, coord1.part)
    range2 = _get_relative_size_range(chimera_hwa, coord2, coord1, coord1.part, other_side_if_equal=True)
    mes1 = [_get_crossroad(coord1, s, d) for s, d in range1()] + [_get_crossroad(coord2, s, d) for s, d in range2()]
    mes2 = [_get_crossroad(coord2, s, d) for s, d in range1()] + [_get_crossroad(coord1, s, d) for s, d in range2()]
    return [mes1, mes2]

def _get_crossroad(coord, row_or_col, dep):
    """ get the crossroad according to node type """
    if Partition.is_horizontal(coord.part):
        return Crossroad(row_or_col, coord.col, dep, coord.dep)
    return Crossroad(coord.row, row_or_col, coord.dep, dep)


# get the sets of mutually exclusive crossroads defined by two broken vertices of different types

def get_mes_different(chimera_hwa, node1, node2):
    """
    get the mutually exclusive sets for two missing nodes of different type

    :param (ChimeraHWA) chimera_hwa: the Chimera graph hardware adjacency
    :param (int) node1: number of first node
    :param (int) node2: number of second node
    :return: lists of nodes forming the mutually exclusive sets
    """
    hori, vert = get_horizontal_and_vertical_coordinates(chimera_hwa, node1, node2)
    if is_in_same_uc_row_or_column(hori, vert):
        # no constraints are necessary
        return []
    common = get_common_crossroad(hori, vert)
    rectangle = _get_mes_rectangle(chimera_hwa, hori, vert)
    all_mes = [[common] + mes for mes in rectangle]
    return all_mes

def _get_mes_rectangle(chimera_hwa, hori, vert):
    """ get the rectangle defined by two broken nodes of different type """
    row_range = list(_get_relative_size_range(chimera_hwa, hori, vert, Partition.HORIZONTAL)())
    col_range = list(_get_relative_size_range(chimera_hwa, vert, hori, Partition.VERTICAL)())
    if len(col_range) < len(row_range):
        return [[Crossroad(row, col, dh, dv) for row, dh in row_range] for col, dv in col_range]
    return [[Crossroad(row, col, dh, dv) for col, dv in col_range] for row, dh in row_range]


# some helper functions

def get_horizontal_and_vertical_coordinates(chimera_hwa, node1, node2):
    """
    get and sort the coordinates of the given nodes

    :param (ChimeraHWA) chimera_hwa: the Chimera graph hardware adjacency
    :param (int) node1: number of first node
    :param (int) node2: number of second node
    :return: the coordinates of the two nodes in a certain ordering
    """
    hori = chimera_hwa.get_coordinate(node1)
    vert = chimera_hwa.get_coordinate(node2)
    if vert.part == hori.part:
        raise ValueError('The nodes should have different orientations')
    if hori.part == Partition.HORIZONTAL:
        return hori, vert
    return vert, hori

def is_in_same_uc_row_or_column(coord1, coord2):
    """
    check if two nodes of the same type lie in either the same unit cell row or column

    :param coord1: the coordinate of the first node
    :param coord2: the coordinate of the second node
    :return: True if the two nodes are in the same unit cell row or column
    """
    return coord1.row == coord2.row or coord1.col == coord2.col

def is_in_same_inner_row_or_col(coord1, coord2):
    """
    check if two nodes of the same type lie in either the same inner row or column

    :param coord1: the coordinate of the first node
    :param coord2: the coordinate of the second node
    :return: True if the two nodes are in the same inner row or column
    """
    return (Partition.is_horizontal(coord1.part) and coord1.col == coord2.col and coord1.dep == coord2.dep) \
           or (not Partition.is_horizontal(coord1.part) and coord1.row == coord2.row and coord1.dep == coord2.dep)

def _get_relative_size_range(chimera_hwa, coord1, coord2, hori_or_vert, other_side_if_equal=False):
    """ get the range of inner row resp. column combination according to relative position of two nodes """
    correction = 0.5 if other_side_if_equal else 0
    if Partition.is_horizontal(hori_or_vert):
        uc_range = get_relative_range(coord1.row, coord2.row - correction, chimera_hwa.size.rows)
    else:
        uc_range = get_relative_range(coord1.col, coord2.col - correction, chimera_hwa.size.cols)
    return product_reusable(uc_range, range(chimera_hwa.size.depth))
