# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for helper objects for defining a specific indexing of the Chimera graph """

from enum import IntEnum
from collections import namedtuple

from cge.utils.ranges import product_of_ranges
from .size import default_size

# helper objects

Coordinate = namedtuple('Coordinate', ['row', 'col', 'part', 'dep'])
UnitCell = namedtuple('UnitCell', ['row', 'col'])

class Partition(IntEnum):
    """
    Usually the two partitions of the unit cells of the Chimera graph are arranged horizontally and vertically.
    We define
        0 - vertically connected nodes -> horizontal partition in unit cell,
        1 - horizontally connected -> vertical partition in unit cell.
    """
    HORIZONTAL = 0
    VERTICAL = 1

    @classmethod
    def is_horizontal(cls, hori_or_vert):
        """ checks whether given value corresponds to horizontal or vertical partition """
        if hori_or_vert not in [cls.HORIZONTAL, cls.VERTICAL]:
            raise ValueError('Is neither horizontal nor vertical')
        return hori_or_vert == cls.HORIZONTAL

    @classmethod
    def get_opposite(cls, hori_or_vert):
        """ get vertical partition if horizontal one was given and vice versa """
        return Partition(1 - Partition(hori_or_vert))


# calculate the coordinates from the node index

@default_size
def get_coordinate(node, size):
    """ calculating the coordinates of the node """
    row, col = get_unit_cell(node, size)  # row and column of unit cell
    dep = node % size.depth
    part = get_partition(node, size)
    return Coordinate(row, col, part, dep)

@default_size
def get_unit_cell(node, size):
    """
    row and column of the unit cell the node is in
    enumeration from left to right and top to bottom, horizontal before vertical partition
    """
    uc_number = node // (size.depth * 2)
    row = uc_number // size.cols
    col = uc_number % size.cols
    return UnitCell(row, col)

@default_size
def get_partition(node, size):
    """ calculate the partition of the node """
    return Partition(node % (size.depth * 2) // size.depth)


# calculate the node index from the coordinates

@default_size
def get_node(row, col, part, dep, size):
    """ full rows above + left full columns in row + nodes in unit cell """
    return row * size.cols * 2 * size.depth + col * 2 * size.depth + part * size.depth + dep

@default_size
def get_full_nodes_in_partition(hori_or_vert, size):
    """ get either the horizontal (hori_or_vert=0) or the vertical nodes (hori_or_vert=1) """
    return [get_node(r, c, hori_or_vert, d, size) for r, c, d in product_of_ranges(*size)]

@default_size
def get_full_horizontal_nodes(size):
    """ get the horizontal nodes """
    return get_full_nodes_in_partition(Partition.HORIZONTAL, size)

@default_size
def get_full_vertical_nodes(size):
    """ get either the vertical nodes """
    return get_full_nodes_in_partition(Partition.VERTICAL, size)


# calculate the corresponding edges

@default_size
def get_edge_in_unit_cell(row, col, dep0, dep1, size):
    """ get an inner unit cell edge of the full (ideal) Chimera graph """
    return get_node(row, col, Partition.HORIZONTAL, dep0, size), \
           get_node(row, col, Partition.VERTICAL, dep1, size)

@default_size
def get_horizontal_edge(row, col, dep, size):
    """ get a horizontal edge of the full (ideal) Chimera graph """
    return get_node(row, col, Partition.VERTICAL, dep, size), \
           get_node(row, col + 1, Partition.VERTICAL, dep, size)

@default_size
def get_vertical_edge(row, col, dep, size):
    """ get a vertical edge of the full (ideal) Chimera graph """
    return get_node(row, col, Partition.HORIZONTAL, dep, size), \
           get_node(row + 1, col, Partition.HORIZONTAL, dep, size)

@default_size
def get_full_edges_in_unit_cell(size):
    """ get all inner unit cell edges of the full (ideal) Chimera graph """
    return [get_edge_in_unit_cell(r, c, d0, d1, size)
            for r, c, d0, d1 in product_of_ranges(*size, size.depth)]

@default_size
def get_full_horizontal_edges(size):
    """ get all horizontal edges of the full (ideal) Chimera graph """
    return [get_horizontal_edge(r, c, d, size)
            for r, c, d in product_of_ranges(size.rows, size.cols - 1, size.depth)]

@default_size
def get_full_vertical_edges(size):
    """ get all vertical edges of the full (ideal) Chimera graph """
    return [get_vertical_edge(r, c, d, size)
            for r, c, d in product_of_ranges(size.rows - 1, size.cols, size.depth)]

@default_size
def get_full_edges(size):
    """ get all edges of the full (ideal) Chimera graph """
    edges = get_full_edges_in_unit_cell(size) + get_full_horizontal_edges(size) + get_full_vertical_edges(size)
    return sorted(edges)


# check if two nodes given by their indices are neighbored

@default_size
def are_neighbored(node1, node2, size):
    """ check if there is an edge in the full (ideal) Chimera graph that connects both nodes """
    c1 = get_coordinate(node1, size)
    c2 = get_coordinate(node2, size)
    return are_neighbored_coordinates(c1, c2)

def are_neighbored_coordinates(coord1, coord2):
    """ check if there is an edge in the full (ideal) Chimera graph that connects both nodes """
    return are_neighbored_in_unit_cell(coord1, coord2) \
           or are_neighbored_horizontally(coord1, coord2) \
           or are_neighbored_vertically(coord1, coord2)

def are_neighbored_in_unit_cell(coord1, coord2):
    """ check if there is an inner unit cell edge in the full (ideal) Chimera graph that connects both nodes """
    return coord1.row == coord2.row and coord1.col == coord2.col and coord1.part != coord2.part

def are_neighbored_horizontally(coord1, coord2):
    """ check if there is an horizontal edge in the full (ideal) Chimera graph that connects both nodes """
    return coord1.row == coord2.row and abs(coord1.col - coord2.col) == 1 and coord1.dep == coord2.dep

def are_neighbored_vertically(coord1, coord2):
    """ check if there is an vertical edge in the full (ideal) Chimera graph that connects both nodes """
    return abs(coord1.row - coord2.row) == 1 and coord1.col == coord2.col and coord1.dep == coord2.dep
