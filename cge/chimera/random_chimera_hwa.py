# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for RandomChimeraHWA """

from random import sample

from .size import default_size
from .chimera_hwa import ChimeraHWA, get_edges_without, get_full_node_number, indexing


class RandomChimeraHWA(ChimeraHWA):
    """ This is a ChimeraHWA derived from deleting certain nodes randomly according to certain parameters. """

    def __init__(self, edges, size, ratio_missing, index=None, name=None):
        """
        initialize ChimeraHWA with missing nodes

        :param (list[tuple]) edges: list of 2-tuples representing edges in the hardware graph
        :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
        :param (float or tuple[float]) ratio_missing: the (intended) ratio of missing nodes to total number of nodes,
                                                      can also be a 2-tuple indicating different probabilities for the
                                                      horizontal and vertical vertex partitions
        :param (int or None) index: additional value to differ between different instances with the same parameters
        :param (str or None) name: name of the HWA for saving different instances
        """
        self.ratio_missing = ratio_missing
        self.index = index
        super().__init__(edges, size, name=name or get_name_from_parameters(size, ratio_missing, index))

    def get_name_from_parameters(self, prefix=''):
        """
        get a nice string representation of the parameters

        :param (str) prefix: a string that shall be added at the beginning
        :return: the string representation
        """
        return get_name_from_parameters(self.size, self.ratio_missing, self.index, prefix)

    @classmethod
    def get_from_parameters(cls, size, ratio_missing=0.0, index=0):
        """
        get a random ChimeraHWA according to the given parameters

        :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
        :param (float or tuple[float]) ratio_missing: the ratio of missing nodes to total number of nodes,
                                                      can also be a 2-tuple indicating different probabilities for the
                                                      horizontal and vertical vertex partitions
        :param (int) index: additional value to differ between different instances with the same parameters
        :return: the random ChimeraHWA
        """
        nodes = get_randomly_missing_nodes(size, ratio_missing)
        edges = get_edges_without(nodes, size)
        return cls(edges, size, ratio_missing, index)

    def get_exact_ratio(self):
        """ get the exact missing nodes ratio which is not always the exact one due to rounding issues """
        return len(self.missing_nodes) / get_full_node_number(self.size)

    def get_exact_split_ratios(self):
        """ get the exact missing nodes ratios which are not always the exact ones due to rounding issues """
        total = get_full_node_number(self.size)
        return 2 * len(self.missing_horizontal_nodes) / total, 2 * len(self.missing_vertical_nodes) / total


def get_randomly_missing_nodes(size, ratio_missing):
    """
    get a random sample of missing nodes according to the desired ratio

    :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
    :param (float or tuple[float]) ratio_missing: the ratio of missing nodes to total number of nodes,
                                                  can also be a 2-tuple indicating different probabilities for the
                                                  horizontal and vertical vertex partitions
    :return: the list of missing nodes
    """
    if isinstance(ratio_missing, tuple):
        return get_randomly_missing_nodes_split_ratios(size, *ratio_missing)
    full_number = get_full_node_number(size)
    total_missing = int(round(ratio_missing * full_number))
    return sample(range(full_number), total_missing)

def get_randomly_missing_nodes_split_ratios(size, ratio_missing_hori, ratio_missing_vert):
    """
    get a random sample of missing nodes according to the desired ratios

    :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
    :param (float) ratio_missing_hori: the ratio of missing horizontal nodes to total number of nodes
    :param (float) ratio_missing_vert: the ratio of missing vertical nodes to total number of nodes
    :return: the list of missing nodes
    """
    full_number = get_full_node_number(size)
    missing_hori = int(round(ratio_missing_hori * full_number / 2))
    missing_vert = int(round(ratio_missing_vert * full_number / 2))
    hori_nodes = indexing.get_full_horizontal_nodes(size)
    vert_nodes = indexing.get_full_vertical_nodes(size)
    return sample(hori_nodes, missing_hori) + sample(vert_nodes, missing_vert)

@default_size
def get_name_from_parameters(size, ratio_missing, index=0, prefix=''):
    """
    get a string representation of the parameters

    :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
    :param (float or tuple[float]) ratio_missing: the (intended) ratio of missing nodes to total number of nodes,
                                                  can also be a 2-tuple indicating different probabilities for the
                                                  horizontal and vertical vertex partitions
    :param (int) index: additional value to differ between different instances with the same parameters
    :param (str) prefix: additional string to be added in front of the concatenated name
    :return: the string representation
    """
    if isinstance(ratio_missing, tuple):
        name = f'size{size.rows:02n}_ratioMissing({ratio_missing[0]:01.3f},{ratio_missing[1]:01.3f})_index{index:01n}'
    else:
        name = f'size{size.rows:02n}_ratioMissing{ratio_missing:01.3f}_index{index:01n}'
    if prefix:
        name = f'{prefix}_' + name
    return name
