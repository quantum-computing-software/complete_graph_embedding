# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for helper functions for the plotting of the Chimera graph """

from math import ceil

from .size import default_size
from .indexing import get_coordinate


@default_size
def get_position(node, size, uc_dist=5, node_dist=1):
    """ position for standard visual representation of node in full Chimera graph """
    coord = get_coordinate(node, size)
    return get_position_from_coord(*coord, size.depth, uc_dist, node_dist)

def get_position_from_coord(row, col, part, dep, depth=4, uc_dist=5, node_dist=1):
    """ position for standard visual representation of node coordinates in full Chimera graph """
    uc_x = uc_dist * col
    uc_y = - uc_dist * row
    if part:  # vertical partition
        return uc_x, uc_y + node_dist * (ceil(depth / 2) - 0.5 - dep)
    # horizontal partition
    return uc_x + node_dist * (dep - ceil(depth / 2) + 0.5), uc_y

def set_default_node_positions(chimera_hwa):
    """ sets the default position for each node """
    positions = {node : get_position(node, chimera_hwa.size) for node in chimera_hwa.graph.nodes}
    chimera_hwa.update_graph({"pos": positions})
