# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for ChimeraHWA """

from functools import cached_property

from quark import HardwareAdjacency

from .size import default_size
from . import indexing


class ChimeraHWA(HardwareAdjacency):
    """ A ChimeraHWA is a special hardware adjacency with specific methods for the Chimera architecture. """

    @default_size
    def __init__(self, edges, size, name=None):
        """
        initialize ChimeraHWA object

        :param (list[tuple]) edges: list of 2-tuples representing edges in the Chimera graph
        :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
        :param (str or None) name: name of the HWA for saving different instances
        """
        self.size = size
        super().__init__(edges, name)

    @cached_property
    def horizontal_nodes(self):
        """ the nodes that lie in the horizontal partition of the unit cell (vertically connected) """
        return sorted(set(self.nodes).intersection(indexing.get_full_horizontal_nodes(self.size)))

    @cached_property
    def vertical_nodes(self):
        """ the nodes that lie in the vertical partition of the unit cell (horizontally connected) """
        return sorted(set(self.nodes).intersection(indexing.get_full_vertical_nodes(self.size)))

    @cached_property
    def missing_nodes(self):
        """ the nodes that are not present here but in the full chimera graph """
        return sorted(set(range(get_full_node_number(self.size))).difference(self.nodes))

    @cached_property
    def missing_horizontal_nodes(self):
        """ the missing horizontal nodes """
        return sorted(set(self.missing_nodes).intersection(indexing.get_full_horizontal_nodes(self.size)))

    @cached_property
    def missing_vertical_nodes(self):
        """ the missing vertical nodes """
        return sorted(set(self.missing_nodes).intersection(indexing.get_full_vertical_nodes(self.size)))

    @cached_property
    def missing_edges(self):
        """ the edges that are not present here but in the full chimera graph """
        return sorted(set(indexing.get_full_edges(self.size)).difference(self))

    @cached_property
    def independent_missing_edges(self):
        """ those edges that are missing but are not incident to a missing node """
        return [edge for edge in self.missing_edges if set(edge).isdisjoint(self.missing_nodes)]

    def get_nodes(self, hori_or_vert=None):
        """
        get the (horizontal/vertical) nodes

        :param (Partition or None) hori_or_vert: the desired partition of the nodes, from both partitions if None
        :return: the list of (horizontal/vertical) nodes
        """
        if hori_or_vert is None:
            return self.nodes
        if indexing.Partition.is_horizontal(hori_or_vert):
            return self.horizontal_nodes
        return self.vertical_nodes

    def get_missing_nodes(self, hori_or_vert=None):
        """
        get the missing (horizontal/vertical) nodes

        :param (Partition or None) hori_or_vert: the desired partition of the nodes, from both partitions if None
        :return: the list of missing (horizontal/vertical) nodes
        """
        if hori_or_vert is None:
            return self.missing_nodes
        if indexing.Partition.is_horizontal(hori_or_vert):
            return self.missing_horizontal_nodes
        return self.missing_vertical_nodes

    def get_coordinate(self, node):
        """
        get the coordinate of a node,
        which is given by the unit cell row and column, the partition and the index in the partition

        :param (int) node: the number of the node in a consecutive enumeration of the nodes
        :return: the tuple representing the coordinate of the node
        """
        return indexing.get_coordinate(node, self.size)

    def get_unit_cell(self, node):
        """
        get the row and the column of the unit cell the node lies in

        :param (int) node: the number of the node in a consecutive enumeration of the nodes
        :return: the tuple with the unit cell coordinates of the node
        """
        return indexing.get_unit_cell(node, self.size)

    def get_partition(self, node):
        """
        get the partition of the node, where
            - 0 -> vertically connected nodes -> horizontal partition in unit cell
            - 1 -> horizontally connected -> vertical partition in unit cell

        :param (int) node: the number of the node in a consecutive enumeration of the nodes
        :return: the partition of the node
        """
        return indexing.get_partition(node, self.size)

    def get_node(self, row, col, part, dep):
        """
        get the node number in a consecutive enumeration of all nodes from the given coordinates

        :param (int) row: the row of the unit cell
        :param (int) col: the column of the unit cell
        :param (Partition) part: the partition of the node in the unit cell
        :param (int) dep: the index of the node in the partition
        :return: the node number
        """
        return indexing.get_node(row, col, part, dep, self.size)

    def are_neighbored(self, node1, node2):
        """
        check whether two nodes are connected by an edge,
        extends 'HardwareAdjacency.are_neighbored' because of easier check in structured Chimera graph

        :param (int) node1: the number of the first node in a consecutive enumeration of the nodes
        :param (int) node2: the number of the second node in a consecutive enumeration of the nodes
        :return: True if the nodes are neighbored
        """
        return indexing.are_neighbored(node1, node2, self.size) and super().are_neighbored(node1, node2)

    @classmethod
    @default_size
    def get_full(cls, size):
        """
        initialize the full (ideal) ChimeraHWA

        :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
        :return: the full ChimeraHWA of the given size
        """
        name = f"full_chimera_{size.rows}_{size.cols}_{size.depth}"
        edges = indexing.get_full_edges(size)
        return cls(name=name, edges=edges, size=size)

    @classmethod
    @default_size
    def get_without(cls, missing_nodes, size, name='chimera_without_some_nodes'):
        """
        initialize the ChimeraHWA with the given size but without the given nodes

        :param (list[int] or set[int]) missing_nodes: the nodes that shall be dropped in the Chimera graph
        :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
        :param (str) name: the name of the resulting HWA
        :return: the ChimeraHWA without the given nodes
        """
        edges = get_edges_without(missing_nodes, size)
        chimera_hwa = cls(name=name, edges=edges, size=size)
        return chimera_hwa

    @classmethod
    @default_size
    def get_with(cls, included_nodes, size, name='chimera_with_some_nodes'):
        """
        initialize the ChimeraHWA with the given size but only with the given nodes

        :param (list[int] or set[int]) included_nodes: the nodes that only shall be included in the Chimera graph
        :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
        :param (str) name: the name of the resulting HWA
        :return: the ChimeraHWA with only the given nodes
        """
        edges = get_edges_with(included_nodes, size)
        chimera_hwa = cls(name=name, edges=edges, size=size)
        return chimera_hwa


@default_size
def get_full_node_number(size):
    """
    get the number of nodes in a full (ideal) Chimera graph of the given size

    :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
    :return: the full number of nodes in an ideal Chimera graph of the given size
    """
    return size.rows * size.cols * size.depth * 2

@default_size
def get_full_edge_number(size):
    """
    get the number of edges in a full (ideal) Chimera graph

    :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
    :return: the full number of edges in an ideal Chimera graph of the given size
    """
    num_inter_edges = size.rows * size.cols * size.depth * size.depth
    num_intra_edges = size.depth * size.rows * (size.cols - 1) + size.depth * (size.rows - 1) * size.cols
    return num_inter_edges + num_intra_edges

@default_size
def get_edges_without(missing_nodes, size):
    """
    get the edges of a Chimera graph of the given size but without the given nodes

    :param (list[int] or set[int]) missing_nodes: the nodes that shall be dropped in the Chimera graph
    :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
    :return: the edges of a ChimeraHWA without the given nodes
    """
    all_edges = indexing.get_full_edges(size)
    return [edge for edge in all_edges if set(edge).isdisjoint(missing_nodes)]

@default_size
def get_edges_with(included_nodes, size):
    """
    get the edges of a Chimera graph of the given size but only with the given nodes

    :param (list[int] or set[int]) included_nodes: the nodes that only shall be included in the Chimera graph
    :param (cge.chimera.size.Size or tuple or int) size: number of unit cells in rows and columns of Chimera graph
    :return: the edges of a ChimeraHWA with only the given nodes
    """
    all_nodes = set(range(get_full_node_number(size)))
    nodes_to_be_removed = all_nodes.difference(included_nodes)
    edges = get_edges_without(nodes_to_be_removed, size)
    return edges
