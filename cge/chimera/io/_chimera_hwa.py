# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for the IO of the ChimeraHWA """

from quark.io import add, hdf5, hdf5_attributes

from cge import ChimeraHWA


SIZE = 'size'


add.save_load_exists(ChimeraHWA)

@add.static_method(ChimeraHWA)
def get_identifying_attributes():
    """ get the attributes that identify an object of the class """
    return ['name']

@add.self_method(ChimeraHWA)
def write_hdf5(chimera_hwa, group):
    """
    write data in attributes and datasets in the opened group of an HDF5 file

    :param (ChimeraHWA) chimera_hwa: the object whose data shall be stored
    :param (h5py.Group) group: the group to store the data in
    """
    hdf5.save_in(super(ChimeraHWA, chimera_hwa), group)
    hdf5_attributes.write_attribute(group, SIZE, chimera_hwa)

@add.static_method(ChimeraHWA)
def read_hdf5(group):
    """
    read data from attributes and datasets of the opened group of an HDF5 file

    :param (h5py.Group) group: the group to read the data from
    :return: the data as keyword arguments
    """
    init_kwargs = hdf5.load_data_from(super(ChimeraHWA, ChimeraHWA), group)
    init_kwargs[SIZE] = hdf5_attributes.read_attribute(group, SIZE)
    return init_kwargs
