# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for the IO of the RandomChimeraHWA """

from quark.io import add, hdf5, hdf5_attributes

from cge.chimera import RandomChimeraHWA


ATTRS = ['ratio_missing', 'index']


add.save_load_exists(RandomChimeraHWA)

@add.static_method(RandomChimeraHWA)
def get_identifying_attributes():
    """ get the attributes that identify an object of the class """
    return ['name']

@add.self_method(RandomChimeraHWA)
def write_hdf5(random_chimera_hwa, group):
    """
    write data in attributes and datasets in the opened group of an HDF5 file

    :param (RandomChimeraHWA) random_chimera_hwa: the object whose data shall be stored
    :param (h5py.Group) group: the group to store the data in
    """
    hdf5.save_in(super(RandomChimeraHWA, random_chimera_hwa), group)
    hdf5_attributes.write_attributes(group, random_chimera_hwa, *ATTRS)

@add.static_method(RandomChimeraHWA)
def read_hdf5(group):
    """
    read data from attributes and datasets of the opened group of an HDF5 file

    :param (h5py.Group) group: the group to read the data from
    :return: the data as keyword arguments
    """
    init_kwargs = hdf5_attributes.read_attributes(group, *ATTRS)
    init_kwargs.update(**hdf5.load_data_from(super(RandomChimeraHWA, RandomChimeraHWA), group))
    return init_kwargs
