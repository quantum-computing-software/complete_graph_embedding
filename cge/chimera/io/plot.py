# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for plotting the Chimera hardware graph """

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import networkx as nx

from quark.io import add

from cge.chimera import ChimeraHWA
from cge.chimera.positions import set_default_node_positions


GRAPH_DEFAULT_KWARGS = {"node_size": 100,
                        "node_color": 'white',
                        "edgecolors": 'black',
                        "font_size": 7}

GREEN = (146 / 256, 208 / 256, 80 / 256, 1)
BLUE = (0, 176 / 256, 240 / 256, 1)


def save_plot(plot_func, filename, *plot_args, **plot_kwargs):
    """ plot the hardware graph and store it to the given file """
    plot_func(*plot_args, **plot_kwargs)
    plt.savefig(filename)
    plt.close()

def plot_graph(graph, label_nodes=False, **draw_kwargs):
    """ plot some graph """
    _set_graph_defaults(label_nodes, graph.nodes, draw_kwargs)
    plt.axis('off')
    nx.draw(graph, **draw_kwargs)
    plt.gcf().tight_layout()

def _set_graph_defaults(label_nodes, nodes, draw_kwargs):
    """ set the default attributes for plotting """
    for attribute, value in GRAPH_DEFAULT_KWARGS.items():
        draw_kwargs.setdefault(attribute, value)
    if label_nodes:
        draw_kwargs.setdefault('labels', {node: str(node) for node in nodes})

@add.self_method(ChimeraHWA, method_name="plot")
def plot_chimera_hwa(chimera_hwa, **draw_kwargs):
    """ plot the whole hardware graph """
    if not ('node_data' in chimera_hwa.graph.graph and 'pos' in chimera_hwa.graph.graph['node_data']):
        set_default_node_positions(chimera_hwa)
    plt.gcf().set_size_inches(2 * chimera_hwa.size.cols, 2 * chimera_hwa.size.rows)
    plot_graph(chimera_hwa.graph, pos=nx.get_node_attributes(chimera_hwa.graph, 'pos'), **draw_kwargs)

@add.self_method(ChimeraHWA, method_name="plot_embedding")
def plot_chimera_hwa_with_embedding(chimera_hwa, embedding, **draw_kwargs):
    """ plot the whole hardware graph with the embedding """
    plot_chimera_hwa(chimera_hwa, **draw_kwargs)
    _set_embedding_defaults(chimera_hwa, draw_kwargs, embedding)
    for color, (var, nodes) in enumerate(embedding.var_nodes_map.items()):
        subgraph = chimera_hwa.graph.subgraph(nodes)
        num_edges = len(subgraph.edges)
        if embedding.var_edges_map:
            draw_kwargs['edgelist'] = embedding.var_edges_map[var]
            num_edges = len(draw_kwargs['edgelist'])
        nx.draw(subgraph,
                node_color=[color] * len(subgraph.nodes),
                edge_color=[color] * num_edges,
                **draw_kwargs)

def _set_embedding_defaults(chimera_hwa, draw_kwargs, embedding):
    """ set the default attributes for plotting """
    num_vars = len(embedding.var_nodes_map)
    cm = get_color_map(num_vars)
    draw_kwargs.setdefault('cmap', cm)
    draw_kwargs.setdefault('vmin', 0)
    draw_kwargs.setdefault('vmax', num_vars - 1)
    draw_kwargs.setdefault('edge_cmap', cm)
    draw_kwargs.setdefault('edge_vmin', 0)
    draw_kwargs.setdefault('edge_vmax', num_vars - 1)
    draw_kwargs.setdefault('width', 4)
    draw_kwargs.setdefault('node_size', 120)
    draw_kwargs.setdefault('pos', nx.get_node_attributes(chimera_hwa.graph, 'pos'))

def get_color_map(number_of_nodes):
    """
    get a specific color map,
    trying to differ between the embeddings of different nodes (could still be improved)
    """
    colors = plt.cm.hsv(np.linspace(0, 0.9, 512))  # pylint: disable=no-member
    if number_of_nodes < 3:
        colors = np.vstack([GREEN, BLUE])
    elif number_of_nodes < 20:
        colors = np.vstack(colors)
    else:
        colors_075 = [np.hstack((0.75 * color[:3], np.array([color[3]]))) for color in colors]
        colors_050 = [np.hstack((0.50 * color[:3], np.array([color[3]]))) for color in colors]
        colors = np.vstack((colors, colors_075, colors_050))
    cm = mpl.colors.LinearSegmentedColormap.from_list('cm_chains', colors)
    return cm
