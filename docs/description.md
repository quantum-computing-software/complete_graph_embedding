# Description

See publication [Embedding of complete graphs in broken Chimera graphs](https://doi.org/10.1007/s11128-021-03168-z) for detailed description
of the underlying optimization problem and the derived heuristic version. 

Define your **`ChimeraHWA`** and pass it to **`embed.get_max_complete_graph_embedding`** to obtain an embedding of the largest complete graph which is embeddable in the corresponding Chimera graph. 
To use the heuristic approach chose a value for **`max_rectangle_ratio`** below 1.0. 
