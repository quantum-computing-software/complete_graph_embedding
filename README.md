# cge - Complete Graph Embedding

[![pipeline status](https://gitlab.com/quantum-computing-software/complete_graph_embedding//badges/development/pipeline.svg)](https://gitlab.com/quantum-computing-software/complete_graph_embedding/-/commits/development)

This is a software package to find the embedding of complete graphs in broken Chimera graphs.

## Documentation

The full documentation can be found [here](https://quantum-computing-software.gitlab.io/complete_graph_embedding/).

## Description

See publication [Embedding of complete graphs in broken Chimera graphs](https://doi.org/10.1007/s11128-021-03168-z) for detailed description
of the underlying optimization problem and the derived heuristic version. 

Define your **`ChimeraHWA`** and pass it to **`embed.get_max_complete_graph_embedding`** to obtain an embedding of the largest complete graph which is embeddable in the corresponding Chimera graph. 
To use the heuristic approach chose a value for **`max_rectangle_ratio`** below 1.0.

## License

This project is [Apache-2.0](https://gitlab.com/quantum-computing-software/complete_graph_embedding/-/blob/development/LICENSE) licensed.

Copyright © 2025 German Aerospace Center (DLR) - Institute of Software Technology (SC). 

Please find the individual contributors [here](https://gitlab.com/quantum-computing-software/complete_graph_embedding/-/blob/development/CONTRIBUTORS) 
and information for citing this package [here](https://gitlab.com/quantum-computing-software/complete_graph_embedding/-/blob/development/CITATION.cff).
