# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the IO of the RandomChimeraHWA """

import os
import pytest

from quark.io import hdf5

from cge.chimera import RandomChimeraHWA


TEST_DIR = os.path.dirname(os.path.abspath(__file__)) + '/../'
FILENAME_TMP = TEST_DIR + "random_chimera_hwa.h5"
FILENAME_FIX = TEST_DIR + 'testdata/random_chimera_hwa.h5'
RANDOM_CHIMERA_HWA = RandomChimeraHWA.get_from_parameters(4, 0.2)
NAME = 'size04_ratioMissing0.200_index0'


def test_load_hdf5():
    """ test loading from h5 files """
    loaded = hdf5.load(RandomChimeraHWA, FILENAME_FIX, name=RANDOM_CHIMERA_HWA.name)
    _check_random_chimera_hwa(loaded)
    assert loaded.missing_nodes == [11, 17, 18, 24, 25, 28, 29, 43, 46, 53, 54, 66, 78,
                                    85, 86, 90, 91, 92, 93, 95, 96, 107, 108, 110, 118, 123]

def test_io_hdf5():
    """ test io round trip """
    hdf5.save(RANDOM_CHIMERA_HWA, FILENAME_TMP)

    loaded = hdf5.load(RandomChimeraHWA, FILENAME_TMP, name=NAME)
    _check_random_chimera_hwa(loaded)
    assert loaded == RANDOM_CHIMERA_HWA

    with pytest.warns(UserWarning, match='Experimental parsing of attributes from group name'):
        loaded = hdf5.load(RandomChimeraHWA, FILENAME_TMP, full_group_name='RandomChimeraHWA/' + NAME)
        _check_random_chimera_hwa(loaded)
        assert loaded == RANDOM_CHIMERA_HWA

    os.remove(FILENAME_TMP)

def _check_random_chimera_hwa(loaded):
    assert loaded.name == NAME
    assert loaded.ratio_missing == 0.2
    assert loaded.index == 0
    assert loaded.size.rows == RANDOM_CHIMERA_HWA.size.rows == 4
    assert loaded.size.cols == RANDOM_CHIMERA_HWA.size.cols == 4
    assert loaded.size.depth == RANDOM_CHIMERA_HWA.size.depth == 4
