# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the largest complete graph embedding by compression with IO """

import os

from quark.io import hdf5

from cge import ChimeraHWA, get_max_complete_graph_embedding_by_compression
import cge.chimera.io  # pylint: disable=unused-import


TEST_DIR = os.path.dirname(os.path.abspath(__file__)) + '/../'
FILENAME = TEST_DIR + '/testdata/hardware_adjacencies.h5'

def test_boothby_compressed():
    """ test embedding in Boothby architecture """
    boothby_hwa = hdf5.load(ChimeraHWA, FILENAME, name="Boothby_512Q")
    decompressed_emb = get_max_complete_graph_embedding_by_compression(boothby_hwa)
    assert len(decompressed_emb.var_nodes_map) == 20
