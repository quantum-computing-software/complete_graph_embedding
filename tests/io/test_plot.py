# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the plotting of the Chimera graph """

import os

from quark import Embedding

from cge import ChimeraHWA
from cge.chimera.io.plot import save_plot, plot_chimera_hwa, plot_chimera_hwa_with_embedding, get_color_map, GREEN
# import cge.chimera.io  # pylint: disable=unused-import

if "PYCHARM_HOSTED" in os.environ:
    # unfortunately, we need this hack to have the tests run locally with PyCharm
    import matplotlib as mpl
    mpl.use('TkAgg')

TEST_DIR = os.path.dirname(os.path.abspath(__file__)) + '/../'
PLOT_FILENAME = TEST_DIR + 'plotting.svg'
CHIMERA_HWA = ChimeraHWA.get_full((3, 5))
VAR_NODES_MAP = {0: [4, 0, 40, 80],
                 1: [5, 1, 41, 81],
                 2: [6, 2, 42, 82],
                 3: [7, 3, 43, 83],
                 4: [44, 52, 48, 88],
                 5: [45, 53, 49, 89],
                 6: [46, 54, 50, 90],
                 7: [47, 55, 51, 91],
                 8: [100, 96, 92, 84],
                 9: [101, 97, 93, 85],
                 10: [102, 98, 94, 86],
                 11: [103, 99, 95, 87]}
EMBEDDING = Embedding.get_from_hwa(VAR_NODES_MAP, CHIMERA_HWA)

def test_plot_chwa():
    """ test plotting of Chimera HWA """
    full_chimera_hwa = ChimeraHWA.get_full((12, 16, 4))
    plot_chimera_hwa(full_chimera_hwa, label_nodes=True)

def test_plot_emb():
    """ test plotting of Chimera HWA with an embedding """
    plot_chimera_hwa_with_embedding(CHIMERA_HWA, EMBEDDING)

def test_save_plot_chwa():
    """ test plotting of Chimera HWA """
    full_chimera_hwa = ChimeraHWA.get_full((12, 16, 4))
    save_plot(plot_chimera_hwa, PLOT_FILENAME, full_chimera_hwa, label_nodes=True)
    assert os.path.exists(PLOT_FILENAME)
    os.remove(PLOT_FILENAME)

def test_save_plot_emb():
    """ test plotting of Chimera HWA with an embedding """
    save_plot(plot_chimera_hwa_with_embedding, PLOT_FILENAME, CHIMERA_HWA, EMBEDDING)
    assert os.path.exists(PLOT_FILENAME)
    os.remove(PLOT_FILENAME)

def test_get_color_map():
    """ test getting color map """
    assert get_color_map(2)(0) == GREEN
    assert get_color_map(15)(0) == (1.0, 0.0, 0.0, 1.0)
    assert get_color_map(25)(0) == (1.0, 0.0, 0.0, 1.0)
