# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the IO of the ChimeraHWA """

import os

from quark.io import hdf5

from cge import ChimeraHWA
import cge.chimera.io  # pylint: disable=unused-import


TEST_DIR = os.path.dirname(os.path.abspath(__file__)) + '/../'
FILENAME_TMP = TEST_DIR + 'chimera_hwa.h5'
FILENAME_FIX = TEST_DIR + 'testdata/chimera_hwa.h5'
FILENAME_HARDWARE = TEST_DIR + '/testdata/hardware_adjacencies.h5'
CHIMERA_HWA = ChimeraHWA.get_without([44, 106], (3, 5))


def test_load_hdf5():
    """ test loading from h5 files """
    loaded = hdf5.load(ChimeraHWA, FILENAME_FIX, name=CHIMERA_HWA.name)
    _check_chimera_hwa(loaded)

def test_io_hdf5():
    """ test io round trip """
    hdf5.save(CHIMERA_HWA, FILENAME_TMP)
    loaded = hdf5.load(ChimeraHWA, FILENAME_TMP, name=CHIMERA_HWA.name)
    _check_chimera_hwa(loaded)
    os.remove(FILENAME_TMP)

def _check_chimera_hwa(loaded):
    assert loaded.name == CHIMERA_HWA.name
    assert loaded == CHIMERA_HWA
    assert loaded.size.rows == CHIMERA_HWA.size.rows == 3
    assert loaded.size.cols == CHIMERA_HWA.size.cols == 5
    assert loaded.size.depth == CHIMERA_HWA.size.depth == 4

def test_current_hardware_independent_missing_edges():
    """ test the extraction of the independent missing edges """
    chimera_hwa = hdf5.load(ChimeraHWA, FILENAME_HARDWARE, name="DW_2000Q_6")
    assert chimera_hwa.independent_missing_edges == [(14, 22), (554, 556)]
