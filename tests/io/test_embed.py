# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the largest complete graph embedding with IO """

import os

from quark import Embedding
from quark.io import hdf5

from cge import ChimeraHWA, get_max_complete_graph_embedding
from cge.funcs.embed import get_embedding_model, extract_embedding
from cge.chimera.mutually_exclusive_sets import get_commons_with_too_large_rectangles


TEST_DIR = os.path.dirname(os.path.abspath(__file__)) + '/../'
FILENAME_HWAS = TEST_DIR + '/testdata/hardware_adjacencies.h5'
FILENAME_EMBEDDINGS = TEST_DIR + '/testdata/embeddings.h5'

NAME = "Boothby_512Q"
NAME_HEURISTIC = NAME + "_0.0"


def test_boothby_objective_value():
    """ test the size of the largest complete graph in Boothby graph """
    boothby_hwa = hdf5.load(ChimeraHWA, FILENAME_HWAS, name=NAME)
    embedding = get_max_complete_graph_embedding(boothby_hwa)
    assert len(embedding.var_nodes_map) == 32
    assert embedding.name == boothby_hwa.name

    exp_embedding = hdf5.load(Embedding, FILENAME_EMBEDDINGS, name=NAME)
    assert embedding == exp_embedding

def test_boothby_heuristic():
    """ test the size of the largest complete graph in Boothby graph with heuristically removed commons """
    boothby_hwa = hdf5.load(ChimeraHWA, FILENAME_HWAS, name=NAME)
    model = get_embedding_model(boothby_hwa, 0.0)
    solution = model.solve()
    assert solution.name == NAME_HEURISTIC

    activated = [var for var, value in solution.items() if value == 1]
    assert len(activated) == 32

    bad_crossroads = get_commons_with_too_large_rectangles(boothby_hwa, 0.0)
    assert set(activated).isdisjoint(bad_crossroads)

    embedding = extract_embedding(solution, boothby_hwa)
    assert len(embedding.var_nodes_map) == 32
    assert embedding.name == NAME_HEURISTIC

    exp_embedding = hdf5.load(Embedding, FILENAME_EMBEDDINGS, name=NAME_HEURISTIC)
    assert embedding == exp_embedding

# def test_current_hardware():
#     """ test the size of the largest complete graph in current hardware """
#     chimera_hwa = hdf5.load(ChimeraHWA, FILENAME_HWAS, name="DW_2000Q_6")
#     with pytest.warns(UserWarning, match="There are missing edges"):
#         embedding = get_max_complete_graph_embedding(chimera_hwa)
#     assert len(embedding.var_nodes_map) == 64
