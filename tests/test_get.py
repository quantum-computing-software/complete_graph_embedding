# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the largest complete graph embedding """

from cge import ChimeraHWA, get_max_complete_graph_embedding, get_max_complete_graph_embedding_by_compression
from cge.chimera import RandomChimeraHWA


NAME = 'test_hwa'


def test_embedding_only_horizontal_broken():
    """ test embedding optimization with only broken horizontal nodes """
    missing_nodes = [0, 1, 2, 3, 24, 25, 26, 27]
    chimera_hwa = ChimeraHWA.get_without(missing_nodes, 2, NAME)
    embedding = get_max_complete_graph_embedding(chimera_hwa)
    exp_var_nodes_map = {0: [4, 8, 12], 1: [5, 9, 13], 2: [6, 10, 14], 3: [7, 11, 15]}
    assert embedding.var_nodes_map == exp_var_nodes_map
    assert embedding.name == NAME

def test_embedding_different_broken():
    """ test embedding optimization with different broken nodes """
    missing_nodes = [4, 5, 6, 7, 24, 25, 26, 27]
    chimera_hwa = ChimeraHWA.get_without(missing_nodes, 2, NAME)
    embedding = get_max_complete_graph_embedding(chimera_hwa)
    exp_var_nodes_map = {0: [0, 16, 20, 28], 1: [1, 17, 21, 29], 2: [2, 18, 22, 30], 3: [3, 19, 23, 31]}
    assert embedding.var_nodes_map == exp_var_nodes_map
    assert embedding.name == NAME

def test_embedding_different_broken_ratio():
    """ test embedding optimization with small rectangle ratio """
    missing_nodes = [0, 1, 2, 3, 28, 29, 30, 31]
    chimera_hwa = ChimeraHWA.get_without(missing_nodes, 2, NAME)
    embedding = get_max_complete_graph_embedding(chimera_hwa, max_rectangle_ratio=0)
    exp_var_nodes_map = {0: [4, 8, 12, 24], 1: [5, 9, 13, 25], 2: [6, 10, 14, 26], 3: [7, 11, 15, 27]}
    assert embedding.var_nodes_map == exp_var_nodes_map
    assert embedding.name == 'test_hwa_0.0'

def test_random_chimera():
    """ test a random instance and check the validity of the returned embedding """
    random_chimera_hwa = RandomChimeraHWA.get_from_parameters(8, 0.1)
    embedding = get_max_complete_graph_embedding(random_chimera_hwa, max_rectangle_ratio=0)
    assert embedding.is_valid('complete')
    assert embedding.name == 'size08_ratioMissing0.100_index0_0.0'

def test_embedding_compressed():
    """ test the optimization of the embedding with a compressed chimera graph """
    chimera_hwa = ChimeraHWA.get_without([17, 20], 2, NAME)
    embedding = get_max_complete_graph_embedding_by_compression(chimera_hwa)
    assert len(embedding.var_nodes_map) == 8
    assert embedding.name == 'test_hwa_compressed_decompressed'
