# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the helper functions for ranges """

from cge.utils.ranges import (product_reusable,
                              product_of_ranges_reusable,
                              get_relative_range,
                              get_relative_start_end,
                              get_length_range)


def test_product_reusable():
    """ test reusable product """
    list1 = [1, 2, 3]
    list2 = [4, 5]
    prod = product_reusable(list1, list2)
    assert list(prod()) == [(1, 4), (1, 5), (2, 4), (2, 5), (3, 4), (3, 5)]
    assert list(prod()) == [(1, 4), (1, 5), (2, 4), (2, 5), (3, 4), (3, 5)]

def test_product_of_ranges_reusable():
    """ test reusable product of ranges """
    prod = product_of_ranges_reusable(3, 2)
    assert list(prod()) == [(0, 0), (0, 1), (1, 0), (1, 1), (2, 0), (2, 1)]
    assert list(prod()) == [(0, 0), (0, 1), (1, 0), (1, 1), (2, 0), (2, 1)]

def test_get_relative_range():
    """ test relative range """
    assert get_relative_range(1, 10, 20) == range(0, 2)
    assert get_relative_range(10, 1, 20) == range(10, 20)
    assert get_relative_range(10, 10, 20) == range(0, 11)

def test_get_relative_start_end():
    """ test start and end of range """
    assert get_relative_start_end(1, 10, 20) == (0, 2)
    assert get_relative_start_end(10, 1, 20) == (10, 20)
    assert get_relative_start_end(10, 10, 20) == (0, 11)

def test_get_length_range():
    """ test relative difference of given values """
    assert get_length_range(1, 10, 20) == 2
    assert get_length_range(10, 1, 20) == 10
    assert get_length_range(10, 10, 20) == 11
