# Copyright 2024 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the cutting of the complete graph embedding """

from quark import Embedding

from cge import ChimeraHWA, get_min_cut_embedding
from cge.funcs.extract import get_crosses_max_bounds, extract_embedding_base
from cge.funcs.cut import get_cut_embedding_heuristic, get_min_bounds, get_cheapest_extension


CHIMERA_HWA = ChimeraHWA.get_without([19], (2, 2, 4))
VAR_NODES_MAP = [[15, 7, 8, 24], [3, 4, 12], [1, 5, 17, 13], [16, 0, 23, 31]]
EXP_VAR_NODES_MAP = {0: [7, 8, 15], 1: [3, 4], 2: [1, 5], 3: [0, 16, 23]}


def test_cut_embedding_heuristic():
    """ test cut embedding heuristic """
    embedding = Embedding.get_from_hwa(VAR_NODES_MAP, CHIMERA_HWA)
    cut_embedding = get_cut_embedding_heuristic(embedding, CHIMERA_HWA)
    assert cut_embedding.var_nodes_map == EXP_VAR_NODES_MAP

def test_min_cut_embedding():
    """ test cut embedding IP """
    embedding = Embedding.get_from_hwa(VAR_NODES_MAP, CHIMERA_HWA)
    cut_embedding = get_min_cut_embedding(embedding, CHIMERA_HWA)
    assert cut_embedding.var_nodes_map == EXP_VAR_NODES_MAP

def test_min_cut_embedding_minmax():
    """ test cut embedding IP with min-max option """
    missing_nodes = [0, 17, 24, 31, 43, 13]
    chimera_hwa = ChimeraHWA.get_without(missing_nodes, (3, 3, 4), 'test')
    crossroads = [(0, 1, 2, 2), (2, 2, 3, 2), (0, 1, 0, 3), (1, 2, 2, 1), (1, 1, 3, 0), (2, 2, 2, 3),
                  (2, 0, 1, 0), (2, 0, 0, 1), (0, 1, 3, 1), (1, 0, 0, 3), (1, 0, 1, 2), (0, 2, 1, 0)]
    embedding = extract_embedding_base(crossroads, get_crosses_max_bounds, chimera_hwa)
    cut_embedding = get_min_cut_embedding(embedding, chimera_hwa, minimize_max_size=True)
    assert cut_embedding.max_map_size == 5


def test_extract_embedding():
    """ test extract embedding """
    cut_bounds = {(0, 1, 3, 0): (0, 1, 0, 0),
                  (0, 0, 0, 3): (0, 0, 0, 0),
                  (0, 0, 1, 1): (0, 0, 0, 0),
                  (1, 0, 3, 0): (0, 0, 0, 1)}
    cut_embedding = extract_embedding_base(cut_bounds, lambda x, y: x, CHIMERA_HWA)
    assert cut_embedding.var_nodes_map == EXP_VAR_NODES_MAP

def test_get_min_bounds():
    """ test minimal bound generation """
    max_bounds = {(0, 0, 0, 3): (0, 1, 0, 0),
                  (0, 0, 1, 1): (0, 1, 0, 1),
                  (0, 1, 3, 0): (0, 1, 0, 1),
                  (1, 0, 3, 0): (0, 1, 0, 1)}
    exp_min_bounds = {(0, 0, 0, 3): (0, 0, 0, 0),
                      (0, 0, 1, 1): (0, 0, 0, 0),
                      (0, 1, 3, 0): (1, 1, 0, 0),
                      (1, 0, 3, 0): (0, 0, 0, 1)}
    exp_pairs = [((0, 0, 0, 3), (0, 1, 3, 0)), ((0, 0, 1, 1), (0, 1, 3, 0)), ((0, 1, 3, 0), (1, 0, 3, 0))]
    min_bounds, pairs = get_min_bounds(max_bounds)
    assert min_bounds == exp_min_bounds
    assert pairs == exp_pairs

def test_get_cheapest_extension():
    """ test cheapest extension of bound generation """
    min_bounds = {(1, 2, 0, 0): (0, 4, 0, 2),
                  (0, 0, 0, 0): (0, 0, 0, 0),
                  (0, 4, 1, 1): (4, 4, 0, 0),
                  (2, 0, 2, 2): (0, 0, 2, 2),
                  (2, 4, 3, 3): (4, 4, 2, 2)}

    exp_extensions = {(1, 2, 0, 0): (0, 4, 0, 2),
                      (0, 0, 0, 0): (0, 0, 0, 1)}
    extension = get_cheapest_extension((1, 2, 0, 0), (0, 0, 0, 0), min_bounds)
    assert extension == exp_extensions

    exp_extensions = {(1, 2, 0, 0): (0, 4, 0, 2),
                      (0, 4, 1, 1): (4, 4, 0, 1)}
    extension = get_cheapest_extension((1, 2, 0, 0), (0, 4, 1, 1), min_bounds)
    assert extension == exp_extensions

    exp_extensions = {(1, 2, 0, 0): (0, 4, 0, 2),
                      (2, 0, 2, 2): (0, 0, 1, 2)}
    extension = get_cheapest_extension((1, 2, 0, 0), (2, 0, 2, 2), min_bounds)
    assert extension == exp_extensions

    exp_extensions = {(1, 2, 0, 0): (0, 4, 0, 2),
                      (2, 4, 3, 3): (4, 4, 1, 2)}
    extension = get_cheapest_extension((1, 2, 0, 0), (2, 4, 3, 3), min_bounds)
    assert extension == exp_extensions
