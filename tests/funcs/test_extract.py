# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the extraction of the final embedding from the solution """

from quark import Embedding

from cge import ChimeraHWA
from cge.funcs.extract import get_var_nodes_map, get_crosses_max_bounds, recover_bounds, recover_crossroads, \
    get_crossroad


CHIMERA_HWA = ChimeraHWA.get_without([17, 20], 2, 'test_hwa')
BOUNDS = {(0, 0, 0, 0): (0, 1, 0, 1),
          (0, 0, 1, 1): (0, 1, 0, 0),
          (1, 1, 0, 0): (1, 1, 0, 1),
          (1, 1, 1, 1): (0, 1, 0, 1)}
VAR_NODES_MAP = [[0, 4, 12, 16], [1, 5, 13], [8, 24, 28], [9, 21, 25, 29]]
EMBEDDING = Embedding(VAR_NODES_MAP)


def test_get_var_nodes_map():
    """ test node set calculation """
    var_nodes_map = get_var_nodes_map(BOUNDS, CHIMERA_HWA)
    assert var_nodes_map == VAR_NODES_MAP

    cut_bounds = {(0, 0, 3, 0): (0, 0, 0, 0),
                  (0, 0, 1, 1): (0, 0, 0, 0),
                  (0, 1, 0, 3): (0, 1, 0, 0),
                  (1, 0, 0, 3): (0, 0, 0, 1)}
    var_nodes_map = get_var_nodes_map(cut_bounds, CHIMERA_HWA)
    exp_var_nodes_map = [[0, 7], [1, 5], [4, 11, 12], [3, 19, 20]]
    assert var_nodes_map == exp_var_nodes_map

def test_get_crosses_max_bounds():
    """ test cross bound calculation """
    crosses_max_bounds = get_crosses_max_bounds(list(BOUNDS.keys()), CHIMERA_HWA)
    assert crosses_max_bounds == BOUNDS

def test_recover_crossroads():
    """ test crossroad recovery """
    exp_crossroads = {(0, 0, 0, 0), (0, 0, 1, 1), (1, 1, 0, 0), (1, 1, 1, 1)}
    crossroads = recover_crossroads(EMBEDDING, CHIMERA_HWA)
    assert set(crossroads) == exp_crossroads

    assert not get_crossroad([], CHIMERA_HWA)

def test_recover_bounds():
    """ test bounds recovery """
    bounds = recover_bounds(EMBEDDING, CHIMERA_HWA)
    assert bounds == BOUNDS
