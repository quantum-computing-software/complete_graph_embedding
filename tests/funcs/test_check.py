# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the check of given crossroads or embeddings """

from quark import Embedding

from cge import ChimeraHWA
from cge.funcs.check import are_valid_crossroads, is_valid_embedding, are_valid_bounds, form_cross


def test_are_valid_crossroads():
    """ test validity check for crossroads """
    missing_nodes = [0, 1, 2, 3, 24, 25, 26, 27]
    hwa = ChimeraHWA.get_without(missing_nodes, (2, 2, 4), 'hwa_without')
    crossroads_correct = [(0, 1, 0, 0), (0, 1, 1, 1), (0, 1, 2, 2), (0, 1, 3, 3)]
    assert are_valid_crossroads(crossroads_correct, hwa)

    crossroads_wrong = [(0, 1, 0, 0), (0, 1, 2, 2), (1, 0, 1, 1), (1, 0, 3, 3)]
    assert not are_valid_crossroads(crossroads_wrong, hwa)

    missing_nodes = [5, 13, 17, 45, 50, 59, 65]
    hwa = ChimeraHWA.get_without(missing_nodes, (3, 3, 4), 'hwa_without')
    crossroads_correct = [(0, 1, 0, 3), (0, 1, 2, 1), (0, 1, 3, 0),
                          (1, 0, 0, 3), (1, 0, 2, 2), (1, 1, 1, 2), (1, 0, 3, 1),
                          (2, 0, 3, 0), (2, 2, 0, 0), (2, 2, 1, 2), (2, 2, 2, 3)]
    assert are_valid_crossroads(crossroads_correct, hwa)

    crossroads_wrong = [(0, 0, 0, 0), (0, 1, 0, 0), (0, 2, 0, 0)]
    assert not are_valid_crossroads(crossroads_wrong, hwa)

    hwa = ChimeraHWA.get_full((1, 1, 4))
    assert not are_valid_crossroads(crossroads_correct, hwa)

def test_is_valid_embedding():
    """ test validity check for embeddings """
    missing_nodes = []
    hwa = ChimeraHWA.get_without(missing_nodes, (2, 2, 4), 'test')
    var_nodes_map_correct = [[16, 0, 23], [1, 5], [3, 4], [15, 7, 8]]
    embedding_correct = Embedding.get_from_hwa(var_nodes_map_correct, hwa)
    assert is_valid_embedding(embedding_correct, hwa)

    var_nodes_map_wrong = [[16, 23], [1, 5], [3, 4], [15, 7, 8]]
    embedding_wrong = Embedding.get_from_hwa(var_nodes_map_wrong, hwa)
    assert not is_valid_embedding(embedding_wrong, hwa)

    var_nodes_map_wrong = [[16, 23], [1, 5], [3, 4], [7, 8]]
    embedding_wrong = Embedding.get_from_hwa(var_nodes_map_wrong, hwa)
    assert not is_valid_embedding(embedding_wrong, hwa)

def test_are_valid_bounds():
    """ test validity check for bounds """
    bounds_correct = {(0, 0, 0, 0): (0, 1, 0, 1),
                      (0, 0, 1, 1): (0, 1, 0, 0),
                      (1, 1, 0, 0): (1, 1, 0, 1),
                      (1, 1, 1, 1): (0, 1, 0, 1)}
    assert are_valid_bounds(bounds_correct)

    bounds_wrong = {(0, 0, 0, 0): (0, 0, 0, 1),
                    (0, 0, 1, 1): (0, 1, 0, 0),
                    (1, 1, 0, 0): (1, 1, 0, 1),
                    (1, 1, 1, 1): (0, 1, 0, 1)}
    assert not are_valid_bounds(bounds_wrong)

def test_form_cross():
    """ test validity check for crosses """
    hwa = ChimeraHWA.get_full((3, 3, 4))
    nodes_correct = [10, 34, 58, 39]
    assert form_cross(nodes_correct, hwa)

    nodes_wrong = [10, 58, 39]
    assert not form_cross(nodes_wrong, hwa)
