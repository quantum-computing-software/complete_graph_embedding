# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the compression of the Chimera graph to reduce problem size """

from quark import Embedding

from cge import ChimeraHWA
from cge.funcs.compress import compress_chimera_hwa, decompress_embedding


CHIMERA_HWA = ChimeraHWA.get_without([17, 20], 2, 'test_hwa')

def test_compress_chimera_hwa():
    """ test the HWA resulting from the compression of the Chimera graph """
    compressed_hwa = compress_chimera_hwa(CHIMERA_HWA)
    assert compressed_hwa.size == (2, 2, 1)
    assert compressed_hwa.name == 'test_hwa_compressed'
    assert compressed_hwa.missing_nodes == [4, 5]

def test_get_decompressed_embedding():
    """ test decompression of a found embedding """
    var_nodes_map_compressed = [[0, 1, 2], [6, 7, 3]]
    exp_var_nodes_map = {0: [0, 4, 8], 1: [1, 5, 9], 2: [2, 6, 10], 3: [3, 7, 11],
                        4: [24, 28, 12], 5: [25, 29, 13], 6: [26, 30, 14], 7: [27, 31, 15]}
    emb_compressed = Embedding(var_nodes_map_compressed, name='test_embedding')
    embedding = decompress_embedding(emb_compressed, CHIMERA_HWA)
    assert embedding.var_nodes_map == exp_var_nodes_map
