# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the largest complete graph embedding """

import pytest
from pyscipopt import Variable

from cge import ChimeraHWA
from cge.funcs.embed import extract_embedding, check_suitability, get_embedding_model


NAME = 'test_hwa'


def test_extract_embedding():
    """ test embedding calculation """
    chimera_hwa = ChimeraHWA.get_without([9, 17, 20], 2, NAME)
    solution = {(0, 0, 0, 0): 1, (0, 0, 1, 1): 1, (1, 1, 0, 0): 1, (1, 1, 1, 1): 1, (2, 2, 2, 2): 0}
    embedding = extract_embedding(solution, chimera_hwa)
    exp_var_nodes_map = {0: [0, 4, 12, 16], 1: [1, 5, 13], 2: [8, 24, 28], 3: [21, 25, 29]}
    assert embedding.var_nodes_map == exp_var_nodes_map

def test_check_suitability():
    """ check HWA with missing edge """
    chimera_hwa = ChimeraHWA.get_without([9, 17, 20], 2, NAME)
    chimera_hwa.remove((0,4))
    with pytest.warns(UserWarning, match="There are missing edges not being incident to missing vertices."):
        check_suitability(chimera_hwa)

def test_get_embedding_model():
    """ check creation of model """
    chimera_hwa = ChimeraHWA.get_without([9, 17, 20], 2, NAME)
    model = get_embedding_model(chimera_hwa)
    assert len(model.data) == 64
    assert isinstance(model.data[(1, 0, 0, 1)], int)
    assert isinstance(model.data[(0, 0, 0, 0)], Variable)
    assert model.getNConss() == 21
