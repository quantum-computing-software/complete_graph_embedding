# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the assembly the mutually exclusive sets of crossroads """

import pytest

from cge import ChimeraHWA
from cge.chimera.mutually_exclusive_sets import get_commons_with_too_large_rectangles, get_rectangle_sizes, \
    get_mes_both_equal, get_mes_different, get_size_mes_different, _get_size_rectangle


def test_get_commons_with_too_large_rectangles():
    """ test the common crossroads resulting from two broken vertices """
    missing_nodes = [0, 1, 28, 29]
    chwa = ChimeraHWA.get_without(missing_nodes, 2, 'test_hwa')

    commons = get_commons_with_too_large_rectangles(chwa, 0)
    assert commons == [(1, 0, 0, 0), (1, 0, 1, 0), (1, 0, 0, 1), (1, 0, 1, 1)]

    assert not get_commons_with_too_large_rectangles(chwa, 1)

def test_get_rectangles_sizes():
    """ test the size of common crossroads resulting from two broken vertices """
    missing_nodes = [0, 1, 28, 29]
    chwa = ChimeraHWA.get_without(missing_nodes, 2, 'test_hwa')

    common_2_size = get_rectangle_sizes(chwa)
    assert common_2_size == {(1, 0, 0, 0): 1,
                             (1, 0, 1, 0): 1,
                             (1, 0, 0, 1): 1,
                             (1, 0, 1, 1): 1}

def test_get_size_mes_different():
    """ test the size of the rectangle resulting from two broken vertices """
    chwa = ChimeraHWA.get_full((6, 5))

    common, size = get_size_mes_different(chwa, 120, 52)
    assert common == (1, 0, 0, 0)
    assert size == 12

    common, size = get_size_mes_different(chwa, 52, 120)
    assert common == (1, 0, 0, 0)
    assert size == 12

    common, size = get_size_mes_different(chwa, 0, 14)
    assert common == (0, 0, 2, 0)
    assert size == 0

def test_get_size_rectangle():
    """ test the size of the rectangle resulting from two broken vertices """
    chwa = ChimeraHWA.get_full((6, 5))
    coord1 = chwa.get_coordinate(120)
    coord2 = chwa.get_coordinate(52)

    size = _get_size_rectangle(chwa, coord1, coord2)
    assert size == (3, 4)

    # if we revert the ordering we get the wrong rectangle
    size = _get_size_rectangle(chwa, coord2, coord1)
    assert size == (2, 1)

def test_get_mes_both_equal():
    """ test the MES resulting from broken horizontal vertices """
    missing_nodes = [0, 1, 2, 3, 24, 25, 26, 27]
    chwa = ChimeraHWA.get_without(missing_nodes, 2, 'test_hwa')

    mess = get_mes_both_equal(chwa, 0, 24)
    mes1_expected = [(0, 0, 0, 0), (0, 0, 1, 0), (0, 0, 2, 0), (0, 0, 3, 0),
                     (1, 1, 0, 0), (1, 1, 1, 0), (1, 1, 2, 0), (1, 1, 3, 0)]
    mes2_expected = [(0, 1, 0, 0), (0, 1, 1, 0), (0, 1, 2, 0), (0, 1, 3, 0),
                     (1, 0, 0, 0), (1, 0, 1, 0), (1, 0, 2, 0), (1, 0, 3, 0)]
    assert mess == [mes1_expected, mes2_expected]

    assert not get_mes_both_equal(chwa, 0, 16)

    with pytest.raises(ValueError, match='The nodes should have the same orientations'):
        get_mes_both_equal(chwa, 0, 4)

def test_get_mes_different():
    """ test the MES resulting from different broken vertices """
    missing_nodes = [0, 1, 2, 3, 28, 29, 30, 31]
    chwa = ChimeraHWA.get_without(missing_nodes, (3, 2), 'test_hwa')

    mess = get_mes_different(chwa, 0, 28)
    rectangle_expected = [[(1, 0, 0, 0), (0, 1, 0, 0), (0, 1, 0, 1), (0, 1, 0, 2), (0, 1, 0, 3)],
                          [(1, 0, 0, 0), (0, 1, 1, 0), (0, 1, 1, 1), (0, 1, 1, 2), (0, 1, 1, 3)],
                          [(1, 0, 0, 0), (0, 1, 2, 0), (0, 1, 2, 1), (0, 1, 2, 2), (0, 1, 2, 3)],
                          [(1, 0, 0, 0), (0, 1, 3, 0), (0, 1, 3, 1), (0, 1, 3, 2), (0, 1, 3, 3)]]
    assert mess == rectangle_expected

    assert not get_mes_different(chwa, 9, 13)
    assert not get_mes_different(chwa, 0, 14)

    assert len(get_mes_different(chwa, 16, 12)) == 4

    with pytest.raises(ValueError, match='The nodes should have different orientations'):
        get_mes_different(chwa, 0, 8)
