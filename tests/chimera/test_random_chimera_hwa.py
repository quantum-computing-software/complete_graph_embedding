# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the RandomChimeraHWA """

from cge.chimera import RandomChimeraHWA


def test_init():
    """ test initialization of a random Chimera HWA """
    rchwa = RandomChimeraHWA.get_from_parameters(20, 0.2)
    assert rchwa.name == 'size20_ratioMissing0.200_index0'
    assert rchwa.get_name_from_parameters('test') == 'test_size20_ratioMissing0.200_index0'

    exact_ratio = rchwa.get_exact_ratio()
    assert abs(exact_ratio - 0.2) < 0.01

    rchwa_tup = RandomChimeraHWA.get_from_parameters(20, (0.2, 0.3))
    assert rchwa_tup.name == 'size20_ratioMissing(0.200,0.300)_index0'
    exact_ratio = rchwa_tup.get_exact_ratio()
    assert abs(exact_ratio - 0.25) < 0.01

    hori_ratio, vert_ratio = rchwa_tup.get_exact_split_ratios()
    assert abs(hori_ratio - 0.2) < 0.01
    assert abs(vert_ratio - 0.3) < 0.01
