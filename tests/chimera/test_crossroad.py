# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the crossroad and corresponding helper functions  """

import pytest

from cge.chimera import ChimeraHWA, Crossroad
from cge.chimera.crossroad import is_broken, get_all_crossroads, get_common_crossroad


def test_is_broken():
    """ test check if crossroad is broken """
    missing_nodes = [0, 1, 28, 29]
    chwa = ChimeraHWA.get_without(missing_nodes, 2, 'test_hwa')
    assert is_broken((0, 0, 0, 0), chwa)
    assert is_broken(Crossroad(1, 1, 0, 0), chwa)
    assert not is_broken((0, 0, 4, 4), chwa)
    assert not is_broken(Crossroad(1, 1, 4, 4), chwa)

def test_get_all_crossroads():
    """ test all the crossroads """
    crossroads = get_all_crossroads(ChimeraHWA.get_full(1))
    assert crossroads == [(0, 0, 0, 0), (0, 0, 0, 1), (0, 0, 0, 2), (0, 0, 0, 3),
                          (0, 0, 1, 0), (0, 0, 1, 1), (0, 0, 1, 2), (0, 0, 1, 3),
                          (0, 0, 2, 0), (0, 0, 2, 1), (0, 0, 2, 2), (0, 0, 2, 3),
                          (0, 0, 3, 0), (0, 0, 3, 1), (0, 0, 3, 2), (0, 0, 3, 3)]

def test_get_common_crossroad():
    """ test the common crossroad """
    assert get_common_crossroad((1, 0, 0, 0), (0, 1, 1, 4)) == Crossroad(row=0, col=0, vert=4, hori=0)
    assert get_common_crossroad((1, 0, 1, 4), (0, 1, 0, 0)) == (1, 1, 4, 0)

    with pytest.raises(ValueError, match='The nodes should have different orientations'):
        get_common_crossroad((1, 0, 1, 4), (0, 1, 1, 0))
