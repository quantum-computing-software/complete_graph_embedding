# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing ChimeraHWA positions """

from cge.chimera.positions import get_position, get_position_from_coord


SIZE = (3, 5)

def test_get_position():
    """ test the calculation of the node positions for plotting """
    assert get_position_from_coord(0, 0, 0, 0) == (-1.5, 0)
    assert get_position_from_coord(0, 0, 0, 1) == (-0.5, 0)
    assert get_position_from_coord(0, 0, 0, 2) == (0.5, 0)
    assert get_position_from_coord(0, 0, 0, 3) == (1.5, 0)
    assert get_position_from_coord(2, 3, 1, 0) == (15, -8.5)
    assert get_position_from_coord(2, 3, 1, 1) == (15, -9.5)
    assert get_position_from_coord(2, 3, 1, 2) == (15, -10.5)
    assert get_position_from_coord(2, 3, 1, 3) == (15, -11.5)

    assert get_position(0, SIZE) == (-1.5, 0)
    assert get_position(1, SIZE) == (-0.5, 0)
    assert get_position(2, SIZE) == (0.5, 0)
    assert get_position(3, SIZE) == (1.5, 0)
    assert get_position(108, SIZE) == (15, -8.5)
    assert get_position(109, SIZE) == (15, -9.5)
    assert get_position(110, SIZE) == (15, -10.5)
    assert get_position(111, SIZE) == (15, -11.5)
