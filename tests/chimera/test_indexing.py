# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the ChimeraHWA indexing """

import pytest

from cge.chimera.indexing import Partition, get_coordinate, get_node, get_partition, get_unit_cell, \
    get_full_horizontal_nodes, get_full_vertical_nodes, get_full_edges_in_unit_cell, get_full_horizontal_edges, \
    get_full_vertical_edges, get_full_edges, are_neighbored


SIZE = (3, 5)  # corresponds to Chimera with 5 rows and 5 columns and a depth of 4

def test_partition():
    """ test the partition enum """
    assert 0 == Partition.HORIZONTAL
    assert 1 == Partition.VERTICAL
    assert 1 != Partition.HORIZONTAL
    assert 0 != Partition.VERTICAL
    assert Partition.HORIZONTAL == 0
    assert Partition.VERTICAL == 1
    assert Partition.HORIZONTAL != 1
    assert Partition.VERTICAL != 0
    assert Partition.VERTICAL != Partition.HORIZONTAL

def test_is_horizontal():
    """ test check """
    assert Partition.is_horizontal(Partition.HORIZONTAL)
    assert not Partition.is_horizontal(Partition.VERTICAL)
    assert Partition.is_horizontal(0)
    assert not Partition.is_horizontal(1)

    with pytest.raises(ValueError, match='Is neither horizontal nor vertical'):
        Partition.is_horizontal(2)

def test_get_opposite():
    """ test method to get opposite orientation """
    assert Partition.get_opposite(0) == Partition.VERTICAL
    assert Partition.get_opposite(1) == Partition.HORIZONTAL

def test_node_coordinates():
    """ test the mapping of coordinates to node indices and vice versa """
    assert get_coordinate(44, SIZE) == (1, 0, 1, 0)
    assert get_node(1, 0, 1, 0, SIZE) == 44
    assert get_partition(44, SIZE) == 1
    assert get_unit_cell(44, SIZE) == (1, 0)

    assert get_node(2, 3, 0, 2, SIZE) == 106
    assert get_coordinate(106, SIZE) == (2, 3, 0, 2)

def test_get_nodes():
    """ test the construction of the node sets """
    assert get_full_horizontal_nodes((1, 2)) == [0, 1, 2, 3, 8, 9, 10, 11]
    assert get_full_vertical_nodes((1, 2)) == [4, 5, 6, 7, 12, 13, 14, 15]

def test_get_edges():
    """ test the construction of the edge sets """
    assert get_full_edges_in_unit_cell((1, 2)) == [(0, 4), (0, 5), (0, 6), (0, 7),
                                                   (1, 4), (1, 5), (1, 6), (1, 7),
                                                   (2, 4), (2, 5), (2, 6), (2, 7),
                                                   (3, 4), (3, 5), (3, 6), (3, 7),
                                                   (8, 12), (8, 13), (8, 14), (8, 15),
                                                   (9, 12), (9, 13), (9, 14), (9, 15),
                                                   (10, 12), (10, 13), (10, 14), (10, 15),
                                                   (11, 12), (11, 13), (11, 14), (11, 15)]
    assert get_full_horizontal_edges((1, 2)) == [(4, 12), (5, 13), (6, 14), (7, 15)]
    assert get_full_vertical_edges((1, 2)) == []
    assert get_full_edges((1,2)) == [(0, 4), (0, 5), (0, 6), (0, 7),
                                     (1, 4), (1, 5), (1, 6), (1, 7),
                                     (2, 4), (2, 5), (2, 6), (2, 7),
                                     (3, 4), (3, 5), (3, 6), (3, 7),
                                     (4, 12), (5, 13), (6, 14), (7, 15),
                                     (8, 12), (8, 13), (8, 14), (8, 15),
                                     (9, 12), (9, 13), (9, 14), (9, 15),
                                     (10, 12), (10, 13), (10, 14), (10, 15),
                                     (11, 12), (11, 13), (11, 14), (11, 15)]

def test_are_neighbored():
    """ test the neighborhood of nodes """
    assert are_neighbored(0, 6, SIZE)
    assert are_neighbored(3, 43, SIZE)
    assert are_neighbored(14, 6, SIZE)
    assert not are_neighbored(1, 10, SIZE)
