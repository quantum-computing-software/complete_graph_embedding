# Copyright 2023 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the ChimeraHWA """

from cge.chimera.indexing import get_full_edges
from cge.chimera.chimera_hwa import ChimeraHWA, get_full_edge_number, get_full_node_number


CHIMERA_HWA = ChimeraHWA.get_full((3, 5))      # depth is by default 4


def test_get_full():
    """ test construction of a non-broken Chimera graph """
    chimera_hwa = ChimeraHWA.get_full(3)
    assert chimera_hwa.name == 'full_chimera_3_3_4'
    assert chimera_hwa.size.rows == chimera_hwa.size.cols == 3
    assert chimera_hwa.size.depth == 4

    assert len(chimera_hwa.nodes) == get_full_node_number((3, 3, 4)) == 72
    assert len(chimera_hwa) == get_full_edge_number((3, 3)) == 192
    assert chimera_hwa == sorted(get_full_edges((3,)))

def test_node_coordinates():
    """ test the mapping of coordinates to node indices and vice versa """
    assert CHIMERA_HWA.get_coordinate(44) == (1, 0, 1, 0)
    assert CHIMERA_HWA.get_node(1, 0, 1, 0) == 44
    assert CHIMERA_HWA.get_partition(44) == 1
    assert CHIMERA_HWA.get_unit_cell(44) == (1, 0)

    assert CHIMERA_HWA.get_node(2, 3, 0, 2) == 106
    assert CHIMERA_HWA.get_coordinate(106) == (2, 3, 0, 2)

def test_broken():
    """ test construction of broken Chimera graph """
    chimera_hwa = ChimeraHWA.get_without([44, 106], (3, 5))
    assert get_full_node_number((3, 5, 4)) == 120
    assert get_full_edge_number((3, 5, 4)) == 328

    assert chimera_hwa.name == 'chimera_without_some_nodes'

    assert chimera_hwa.missing_nodes == [44, 106]
    assert chimera_hwa.missing_horizontal_nodes == [106]
    assert chimera_hwa.missing_vertical_nodes == [44]
    assert chimera_hwa.get_missing_nodes() == [44, 106]
    assert chimera_hwa.get_missing_nodes(0) == [106]
    assert chimera_hwa.get_missing_nodes(1) == [44]

    assert len(chimera_hwa.nodes) == 118
    assert len(chimera_hwa.horizontal_nodes) == 59
    assert len(chimera_hwa.vertical_nodes) == 59
    assert len(chimera_hwa.get_nodes()) == 118
    assert len(chimera_hwa.get_nodes(0)) == 59
    assert len(chimera_hwa.get_nodes(1)) == 59

    assert len(chimera_hwa) == 318
    assert chimera_hwa.missing_edges == [(40, 44), (41, 44), (42, 44), (43, 44), (44, 52), (66, 106),
                                         (106, 108), (106, 109), (106, 110), (106, 111)]

    chimera_hwa = ChimeraHWA.get_with(range(100), (3, 5))
    assert chimera_hwa.name == 'chimera_with_some_nodes'
    assert len(chimera_hwa.nodes) == 100
    assert chimera_hwa.missing_nodes == list(range(100, 120))

def test_are_neighbored():
    """ test neighborhood of nodes """
    assert CHIMERA_HWA.are_neighbored(0, 6)
    assert CHIMERA_HWA.are_neighbored(3, 43)
    assert CHIMERA_HWA.are_neighbored(14, 6)
    assert not CHIMERA_HWA.are_neighbored(1, 10)

    CHIMERA_HWA.remove((0, 6))
    CHIMERA_HWA.remove((3, 43))
    CHIMERA_HWA.remove((6, 14))
    assert not CHIMERA_HWA.are_neighbored(0, 6)
    assert not CHIMERA_HWA.are_neighbored(3, 43)
    assert not CHIMERA_HWA.are_neighbored(14, 6)

def test_independent_missing_edges():
    """ check HWA with missing edges """
    chimera_hwa = ChimeraHWA.get_without([9, 17, 20], 2, "misssing")
    chimera_hwa.remove((0,4))
    assert chimera_hwa.independent_missing_edges == [(0,4)]
