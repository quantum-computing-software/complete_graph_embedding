""" setup for project quark """

from setuptools import setup, find_packages


setup(name = 'cge',
      version = '0.1',
      url = 'https://gitlab.com/quantum-computing-software/complete_graph_embedding/',
      author = 'DLR-SC',
      author_email = 'qc-software@dlr.de',
      python_requires = '>=3.10',
      description = 'This is a software package to find the embedding of complete graphs in broken Chimera graphs.',
      packages = find_packages(include=['cge', 'cge.chimera', 'cge.funcs', 'cge.utils']),
      zip_safe = False)
